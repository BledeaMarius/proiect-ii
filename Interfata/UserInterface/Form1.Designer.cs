﻿namespace UserInterface
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel_bottom = new System.Windows.Forms.Panel();
            this.panel_left = new System.Windows.Forms.Panel();
            this.treeViewUserMenu = new System.Windows.Forms.TreeView();
            this.panel_right = new System.Windows.Forms.Panel();
            this.button_home = new System.Windows.Forms.Button();
            this.wrong_dates = new System.Windows.Forms.Label();
            this.button_register = new System.Windows.Forms.Button();
            this.login = new System.Windows.Forms.Button();
            this.password = new System.Windows.Forms.TextBox();
            this.user = new System.Windows.Forms.TextBox();
            this.quit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button_personal = new System.Windows.Forms.Button();
            this.button_cart = new System.Windows.Forms.Button();
            this.panel_register = new System.Windows.Forms.Panel();
            this.button_eye = new System.Windows.Forms.Button();
            this.wrong_pass = new System.Windows.Forms.Label();
            this.signup = new System.Windows.Forms.Button();
            this.textBox_confirm_pass = new System.Windows.Forms.TextBox();
            this.textBox_new_pass = new System.Windows.Forms.TextBox();
            this.textBox_new_user = new System.Windows.Forms.TextBox();
            this.panel_top = new System.Windows.Forms.Panel();
            this.panel_personal = new System.Windows.Forms.Panel();
            this.button_pers_modify = new System.Windows.Forms.Button();
            this.button_edit_personal = new System.Windows.Forms.Button();
            this.label_pers_fname = new System.Windows.Forms.Label();
            this.label_mandatory = new System.Windows.Forms.Label();
            this.textBox_pers_fname = new System.Windows.Forms.TextBox();
            this.textBox_pers_lname = new System.Windows.Forms.TextBox();
            this.label_pers_phone = new System.Windows.Forms.Label();
            this.textBox_pers_adress = new System.Windows.Forms.TextBox();
            this.label_pers_adress = new System.Windows.Forms.Label();
            this.textBox_pers_phone = new System.Windows.Forms.TextBox();
            this.label_pers_lname = new System.Windows.Forms.Label();
            this.panel_cos = new System.Windows.Forms.Panel();
            this.labelPretTotal = new System.Windows.Forms.Label();
            this.buttonPlaceOrder = new System.Windows.Forms.Button();
            this.textBoxQuantity = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonStergeProdus = new System.Windows.Forms.Button();
            this.buttonUpdateCantitate = new System.Windows.Forms.Button();
            this.dataGridView_cart = new System.Windows.Forms.DataGridView();
            this.label_cart = new System.Windows.Forms.Label();
            this.panel_center = new System.Windows.Forms.Panel();
            this.panel_left.SuspendLayout();
            this.panel_right.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel_register.SuspendLayout();
            this.panel_personal.SuspendLayout();
            this.panel_cos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_cart)).BeginInit();
            this.panel_center.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_bottom
            // 
            this.panel_bottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_bottom.Location = new System.Drawing.Point(0, 607);
            this.panel_bottom.Name = "panel_bottom";
            this.panel_bottom.Size = new System.Drawing.Size(1224, 44);
            this.panel_bottom.TabIndex = 2;
            // 
            // panel_left
            // 
            this.panel_left.BackColor = System.Drawing.Color.SlateGray;
            this.panel_left.Controls.Add(this.treeViewUserMenu);
            this.panel_left.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel_left.Location = new System.Drawing.Point(0, 0);
            this.panel_left.Name = "panel_left";
            this.panel_left.Size = new System.Drawing.Size(142, 607);
            this.panel_left.TabIndex = 3;
            // 
            // treeViewUserMenu
            // 
            this.treeViewUserMenu.BackColor = System.Drawing.Color.SlateGray;
            this.treeViewUserMenu.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.treeViewUserMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeViewUserMenu.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.treeViewUserMenu.ItemHeight = 22;
            this.treeViewUserMenu.Location = new System.Drawing.Point(0, 76);
            this.treeViewUserMenu.Name = "treeViewUserMenu";
            this.treeViewUserMenu.ShowLines = false;
            this.treeViewUserMenu.ShowPlusMinus = false;
            this.treeViewUserMenu.Size = new System.Drawing.Size(142, 363);
            this.treeViewUserMenu.TabIndex = 0;
            // 
            // panel_right
            // 
            this.panel_right.BackColor = System.Drawing.Color.SlateGray;
            this.panel_right.Controls.Add(this.button_home);
            this.panel_right.Controls.Add(this.wrong_dates);
            this.panel_right.Controls.Add(this.button_register);
            this.panel_right.Controls.Add(this.login);
            this.panel_right.Controls.Add(this.password);
            this.panel_right.Controls.Add(this.user);
            this.panel_right.Controls.Add(this.quit);
            this.panel_right.Controls.Add(this.panel1);
            this.panel_right.Controls.Add(this.panel_register);
            this.panel_right.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel_right.Location = new System.Drawing.Point(1224, 0);
            this.panel_right.Name = "panel_right";
            this.panel_right.Size = new System.Drawing.Size(155, 651);
            this.panel_right.TabIndex = 4;
            // 
            // button_home
            // 
            this.button_home.BackColor = System.Drawing.Color.SlateGray;
            this.button_home.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_home.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_home.FlatAppearance.BorderSize = 0;
            this.button_home.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_home.Image = ((System.Drawing.Image)(resources.GetObject("button_home.Image")));
            this.button_home.Location = new System.Drawing.Point(88, 1000);
            this.button_home.Name = "button_home";
            this.button_home.Size = new System.Drawing.Size(58, 54);
            this.button_home.TabIndex = 12;
            this.button_home.UseVisualStyleBackColor = false;
            this.button_home.Click += new System.EventHandler(this.button_home_Click);
            // 
            // wrong_dates
            // 
            this.wrong_dates.AutoEllipsis = true;
            this.wrong_dates.AutoSize = true;
            this.wrong_dates.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.wrong_dates.Location = new System.Drawing.Point(17, 137);
            this.wrong_dates.Name = "wrong_dates";
            this.wrong_dates.Size = new System.Drawing.Size(0, 13);
            this.wrong_dates.TabIndex = 5;
            this.wrong_dates.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_register
            // 
            this.button_register.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_register.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_register.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_register.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.button_register.Location = new System.Drawing.Point(84, 128);
            this.button_register.Name = "button_register";
            this.button_register.Size = new System.Drawing.Size(62, 30);
            this.button_register.TabIndex = 4;
            this.button_register.Text = "Register";
            this.button_register.UseVisualStyleBackColor = true;
            this.button_register.Click += new System.EventHandler(this.button_register_Click);
            // 
            // login
            // 
            this.login.Cursor = System.Windows.Forms.Cursors.Hand;
            this.login.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.login.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.login.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.login.Location = new System.Drawing.Point(6, 128);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(63, 30);
            this.login.TabIndex = 3;
            this.login.Text = "Login";
            this.login.UseVisualStyleBackColor = true;
            this.login.Click += new System.EventHandler(this.login_Click);
            // 
            // password
            // 
            this.password.BackColor = System.Drawing.Color.AliceBlue;
            this.password.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.password.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.password.Location = new System.Drawing.Point(6, 102);
            this.password.Name = "password";
            this.password.Size = new System.Drawing.Size(140, 20);
            this.password.TabIndex = 2;
            this.password.Text = "Password";
            this.password.UseSystemPasswordChar = true;
            this.password.MouseClick += new System.Windows.Forms.MouseEventHandler(this.password_MouseClick);
            // 
            // user
            // 
            this.user.BackColor = System.Drawing.Color.AliceBlue;
            this.user.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.user.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.user.Location = new System.Drawing.Point(6, 76);
            this.user.Name = "user";
            this.user.Size = new System.Drawing.Size(140, 20);
            this.user.TabIndex = 1;
            this.user.Text = "User";
            this.user.MouseClick += new System.Windows.Forms.MouseEventHandler(this.user_MouseClick);
            // 
            // quit
            // 
            this.quit.BackColor = System.Drawing.Color.SlateGray;
            this.quit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.quit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.quit.FlatAppearance.BorderSize = 0;
            this.quit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.quit.Image = ((System.Drawing.Image)(resources.GetObject("quit.Image")));
            this.quit.Location = new System.Drawing.Point(119, 12);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(18, 18);
            this.quit.TabIndex = 0;
            this.quit.UseVisualStyleBackColor = false;
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_personal);
            this.panel1.Controls.Add(this.button_cart);
            this.panel1.Location = new System.Drawing.Point(0, 76);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(155, 137);
            this.panel1.TabIndex = 0;
            this.panel1.Visible = false;
            // 
            // button_personal
            // 
            this.button_personal.BackColor = System.Drawing.Color.SlateGray;
            this.button_personal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_personal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_personal.FlatAppearance.BorderSize = 0;
            this.button_personal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_personal.Image = ((System.Drawing.Image)(resources.GetObject("button_personal.Image")));
            this.button_personal.Location = new System.Drawing.Point(20, 16);
            this.button_personal.Name = "button_personal";
            this.button_personal.Size = new System.Drawing.Size(58, 54);
            this.button_personal.TabIndex = 8;
            this.button_personal.UseVisualStyleBackColor = false;
            this.button_personal.Click += new System.EventHandler(this.button_personal_Click);
            // 
            // button_cart
            // 
            this.button_cart.BackColor = System.Drawing.Color.SlateGray;
            this.button_cart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_cart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_cart.FlatAppearance.BorderSize = 0;
            this.button_cart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_cart.Image = ((System.Drawing.Image)(resources.GetObject("button_cart.Image")));
            this.button_cart.Location = new System.Drawing.Point(84, 16);
            this.button_cart.Name = "button_cart";
            this.button_cart.Size = new System.Drawing.Size(59, 54);
            this.button_cart.TabIndex = 7;
            this.button_cart.UseVisualStyleBackColor = false;
            this.button_cart.Click += new System.EventHandler(this.button_cart_Click);
            // 
            // panel_register
            // 
            this.panel_register.Controls.Add(this.button_eye);
            this.panel_register.Controls.Add(this.wrong_pass);
            this.panel_register.Controls.Add(this.signup);
            this.panel_register.Controls.Add(this.textBox_confirm_pass);
            this.panel_register.Controls.Add(this.textBox_new_pass);
            this.panel_register.Controls.Add(this.textBox_new_user);
            this.panel_register.Location = new System.Drawing.Point(0, 76);
            this.panel_register.Name = "panel_register";
            this.panel_register.Size = new System.Drawing.Size(155, 137);
            this.panel_register.TabIndex = 9;
            this.panel_register.Visible = false;
            // 
            // button_eye
            // 
            this.button_eye.BackColor = System.Drawing.Color.SlateGray;
            this.button_eye.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.button_eye.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_eye.FlatAppearance.BorderSize = 0;
            this.button_eye.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_eye.Image = ((System.Drawing.Image)(resources.GetObject("button_eye.Image")));
            this.button_eye.Location = new System.Drawing.Point(125, 81);
            this.button_eye.Name = "button_eye";
            this.button_eye.Size = new System.Drawing.Size(18, 18);
            this.button_eye.TabIndex = 6;
            this.button_eye.UseVisualStyleBackColor = false;
            this.button_eye.MouseDown += new System.Windows.Forms.MouseEventHandler(this.button_eye_MouseDown);
            this.button_eye.MouseUp += new System.Windows.Forms.MouseEventHandler(this.button_eye_MouseUp);
            // 
            // wrong_pass
            // 
            this.wrong_pass.AutoSize = true;
            this.wrong_pass.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.wrong_pass.Location = new System.Drawing.Point(3, 87);
            this.wrong_pass.Name = "wrong_pass";
            this.wrong_pass.Size = new System.Drawing.Size(0, 13);
            this.wrong_pass.TabIndex = 10;
            // 
            // signup
            // 
            this.signup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.signup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.signup.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.signup.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.signup.Location = new System.Drawing.Point(45, 104);
            this.signup.Name = "signup";
            this.signup.Size = new System.Drawing.Size(62, 30);
            this.signup.TabIndex = 6;
            this.signup.Text = "Sign Up!";
            this.signup.UseVisualStyleBackColor = true;
            this.signup.Click += new System.EventHandler(this.signup_Click);
            // 
            // textBox_confirm_pass
            // 
            this.textBox_confirm_pass.BackColor = System.Drawing.Color.AliceBlue;
            this.textBox_confirm_pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_confirm_pass.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.textBox_confirm_pass.Location = new System.Drawing.Point(3, 55);
            this.textBox_confirm_pass.Name = "textBox_confirm_pass";
            this.textBox_confirm_pass.Size = new System.Drawing.Size(140, 20);
            this.textBox_confirm_pass.TabIndex = 8;
            this.textBox_confirm_pass.Text = "Confirm Passsword";
            this.textBox_confirm_pass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox_confirm_pass_MouseClick);
            // 
            // textBox_new_pass
            // 
            this.textBox_new_pass.BackColor = System.Drawing.Color.AliceBlue;
            this.textBox_new_pass.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_new_pass.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.textBox_new_pass.Location = new System.Drawing.Point(3, 29);
            this.textBox_new_pass.Name = "textBox_new_pass";
            this.textBox_new_pass.Size = new System.Drawing.Size(140, 20);
            this.textBox_new_pass.TabIndex = 7;
            this.textBox_new_pass.Text = "Password";
            this.textBox_new_pass.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox_new_pass_MouseClick);
            // 
            // textBox_new_user
            // 
            this.textBox_new_user.BackColor = System.Drawing.Color.AliceBlue;
            this.textBox_new_user.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_new_user.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.textBox_new_user.Location = new System.Drawing.Point(3, 3);
            this.textBox_new_user.Name = "textBox_new_user";
            this.textBox_new_user.Size = new System.Drawing.Size(140, 20);
            this.textBox_new_user.TabIndex = 6;
            this.textBox_new_user.Text = "E-mail";
            this.textBox_new_user.MouseClick += new System.Windows.Forms.MouseEventHandler(this.textBox_new_user_MouseClick);
            // 
            // panel_top
            // 
            this.panel_top.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel_top.BackgroundImage")));
            this.panel_top.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel_top.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_top.Location = new System.Drawing.Point(142, 0);
            this.panel_top.Name = "panel_top";
            this.panel_top.Size = new System.Drawing.Size(1082, 76);
            this.panel_top.TabIndex = 5;
            // 
            // panel_personal
            // 
            this.panel_personal.AutoSize = true;
            this.panel_personal.Controls.Add(this.button_pers_modify);
            this.panel_personal.Controls.Add(this.button_edit_personal);
            this.panel_personal.Controls.Add(this.label_pers_fname);
            this.panel_personal.Controls.Add(this.label_mandatory);
            this.panel_personal.Controls.Add(this.textBox_pers_fname);
            this.panel_personal.Controls.Add(this.textBox_pers_lname);
            this.panel_personal.Controls.Add(this.label_pers_phone);
            this.panel_personal.Controls.Add(this.textBox_pers_adress);
            this.panel_personal.Controls.Add(this.label_pers_adress);
            this.panel_personal.Controls.Add(this.textBox_pers_phone);
            this.panel_personal.Controls.Add(this.label_pers_lname);
            this.panel_personal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel_personal.Location = new System.Drawing.Point(3, 0);
            this.panel_personal.Name = "panel_personal";
            this.panel_personal.Size = new System.Drawing.Size(513, 400);
            this.panel_personal.TabIndex = 0;
            this.panel_personal.Visible = false;
            // 
            // button_pers_modify
            // 
            this.button_pers_modify.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button_pers_modify.BackgroundImage")));
            this.button_pers_modify.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_pers_modify.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_pers_modify.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_pers_modify.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_pers_modify.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.button_pers_modify.Location = new System.Drawing.Point(91, 269);
            this.button_pers_modify.Name = "button_pers_modify";
            this.button_pers_modify.Size = new System.Drawing.Size(108, 36);
            this.button_pers_modify.TabIndex = 11;
            this.button_pers_modify.Text = "Modify";
            this.button_pers_modify.UseVisualStyleBackColor = true;
            this.button_pers_modify.Click += new System.EventHandler(this.button_pers_modify_Click);
            // 
            // button_edit_personal
            // 
            this.button_edit_personal.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button_edit_personal.BackgroundImage")));
            this.button_edit_personal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_edit_personal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_edit_personal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_edit_personal.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_edit_personal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.button_edit_personal.Location = new System.Drawing.Point(217, 269);
            this.button_edit_personal.Name = "button_edit_personal";
            this.button_edit_personal.Size = new System.Drawing.Size(101, 36);
            this.button_edit_personal.TabIndex = 10;
            this.button_edit_personal.Text = "Edit";
            this.button_edit_personal.UseVisualStyleBackColor = true;
            this.button_edit_personal.Click += new System.EventHandler(this.button_edit_personal_Click);
            // 
            // label_pers_fname
            // 
            this.label_pers_fname.AutoSize = true;
            this.label_pers_fname.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.label_pers_fname.Location = new System.Drawing.Point(47, 46);
            this.label_pers_fname.Name = "label_pers_fname";
            this.label_pers_fname.Size = new System.Drawing.Size(57, 13);
            this.label_pers_fname.TabIndex = 8;
            this.label_pers_fname.Text = "First Name";
            // 
            // label_mandatory
            // 
            this.label_mandatory.AutoSize = true;
            this.label_mandatory.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.label_mandatory.Location = new System.Drawing.Point(50, 235);
            this.label_mandatory.Name = "label_mandatory";
            this.label_mandatory.Size = new System.Drawing.Size(0, 13);
            this.label_mandatory.TabIndex = 9;
            // 
            // textBox_pers_fname
            // 
            this.textBox_pers_fname.Location = new System.Drawing.Point(142, 39);
            this.textBox_pers_fname.Name = "textBox_pers_fname";
            this.textBox_pers_fname.ReadOnly = true;
            this.textBox_pers_fname.Size = new System.Drawing.Size(176, 20);
            this.textBox_pers_fname.TabIndex = 0;
            // 
            // textBox_pers_lname
            // 
            this.textBox_pers_lname.Location = new System.Drawing.Point(142, 82);
            this.textBox_pers_lname.Name = "textBox_pers_lname";
            this.textBox_pers_lname.ReadOnly = true;
            this.textBox_pers_lname.Size = new System.Drawing.Size(176, 20);
            this.textBox_pers_lname.TabIndex = 1;
            // 
            // label_pers_phone
            // 
            this.label_pers_phone.AutoSize = true;
            this.label_pers_phone.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.label_pers_phone.Location = new System.Drawing.Point(48, 200);
            this.label_pers_phone.Name = "label_pers_phone";
            this.label_pers_phone.Size = new System.Drawing.Size(38, 13);
            this.label_pers_phone.TabIndex = 7;
            this.label_pers_phone.Text = "Phone";
            // 
            // textBox_pers_adress
            // 
            this.textBox_pers_adress.Location = new System.Drawing.Point(142, 128);
            this.textBox_pers_adress.Multiline = true;
            this.textBox_pers_adress.Name = "textBox_pers_adress";
            this.textBox_pers_adress.ReadOnly = true;
            this.textBox_pers_adress.Size = new System.Drawing.Size(176, 40);
            this.textBox_pers_adress.TabIndex = 2;
            // 
            // label_pers_adress
            // 
            this.label_pers_adress.AutoSize = true;
            this.label_pers_adress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.label_pers_adress.Location = new System.Drawing.Point(47, 135);
            this.label_pers_adress.Name = "label_pers_adress";
            this.label_pers_adress.Size = new System.Drawing.Size(45, 13);
            this.label_pers_adress.TabIndex = 6;
            this.label_pers_adress.Text = "Address";
            // 
            // textBox_pers_phone
            // 
            this.textBox_pers_phone.Location = new System.Drawing.Point(142, 193);
            this.textBox_pers_phone.Name = "textBox_pers_phone";
            this.textBox_pers_phone.ReadOnly = true;
            this.textBox_pers_phone.Size = new System.Drawing.Size(176, 20);
            this.textBox_pers_phone.TabIndex = 3;
            // 
            // label_pers_lname
            // 
            this.label_pers_lname.AutoSize = true;
            this.label_pers_lname.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.label_pers_lname.Location = new System.Drawing.Point(47, 89);
            this.label_pers_lname.Name = "label_pers_lname";
            this.label_pers_lname.Size = new System.Drawing.Size(58, 13);
            this.label_pers_lname.TabIndex = 5;
            this.label_pers_lname.Text = "Last Name";
            // 
            // panel_cos
            // 
            this.panel_cos.Controls.Add(this.labelPretTotal);
            this.panel_cos.Controls.Add(this.buttonPlaceOrder);
            this.panel_cos.Controls.Add(this.textBoxQuantity);
            this.panel_cos.Controls.Add(this.label1);
            this.panel_cos.Controls.Add(this.buttonStergeProdus);
            this.panel_cos.Controls.Add(this.buttonUpdateCantitate);
            this.panel_cos.Controls.Add(this.dataGridView_cart);
            this.panel_cos.Controls.Add(this.label_cart);
            this.panel_cos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.panel_cos.Location = new System.Drawing.Point(550, 6);
            this.panel_cos.Name = "panel_cos";
            this.panel_cos.Size = new System.Drawing.Size(409, 413);
            this.panel_cos.TabIndex = 1;
            this.panel_cos.Visible = false;
            // 
            // labelPretTotal
            // 
            this.labelPretTotal.AutoSize = true;
            this.labelPretTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPretTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(255)))));
            this.labelPretTotal.Location = new System.Drawing.Point(252, 283);
            this.labelPretTotal.Name = "labelPretTotal";
            this.labelPretTotal.Size = new System.Drawing.Size(66, 16);
            this.labelPretTotal.TabIndex = 7;
            this.labelPretTotal.Text = "Pret Total";
            // 
            // buttonPlaceOrder
            // 
            this.buttonPlaceOrder.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonPlaceOrder.BackgroundImage")));
            this.buttonPlaceOrder.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPlaceOrder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlaceOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonPlaceOrder.Location = new System.Drawing.Point(255, 369);
            this.buttonPlaceOrder.Name = "buttonPlaceOrder";
            this.buttonPlaceOrder.Size = new System.Drawing.Size(151, 36);
            this.buttonPlaceOrder.TabIndex = 6;
            this.buttonPlaceOrder.Text = "Place Order";
            this.buttonPlaceOrder.UseVisualStyleBackColor = true;
            this.buttonPlaceOrder.Click += new System.EventHandler(this.buttonPlaceOrder_Click);
            // 
            // textBoxQuantity
            // 
            this.textBoxQuantity.BackColor = System.Drawing.SystemColors.Control;
            this.textBoxQuantity.Location = new System.Drawing.Point(16, 299);
            this.textBoxQuantity.Name = "textBoxQuantity";
            this.textBoxQuantity.Size = new System.Drawing.Size(30, 20);
            this.textBoxQuantity.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.label1.Location = new System.Drawing.Point(13, 281);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Select product and enter quantity";
            // 
            // buttonStergeProdus
            // 
            this.buttonStergeProdus.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonStergeProdus.BackgroundImage")));
            this.buttonStergeProdus.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonStergeProdus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStergeProdus.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonStergeProdus.Location = new System.Drawing.Point(255, 325);
            this.buttonStergeProdus.Name = "buttonStergeProdus";
            this.buttonStergeProdus.Size = new System.Drawing.Size(151, 36);
            this.buttonStergeProdus.TabIndex = 3;
            this.buttonStergeProdus.Text = "Delete Product";
            this.buttonStergeProdus.UseVisualStyleBackColor = true;
            this.buttonStergeProdus.Click += new System.EventHandler(this.buttonStergeProdus_Click);
            // 
            // buttonUpdateCantitate
            // 
            this.buttonUpdateCantitate.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonUpdateCantitate.BackgroundImage")));
            this.buttonUpdateCantitate.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonUpdateCantitate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpdateCantitate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonUpdateCantitate.Location = new System.Drawing.Point(16, 325);
            this.buttonUpdateCantitate.Name = "buttonUpdateCantitate";
            this.buttonUpdateCantitate.Size = new System.Drawing.Size(161, 36);
            this.buttonUpdateCantitate.TabIndex = 2;
            this.buttonUpdateCantitate.Text = "Update Cantitate";
            this.buttonUpdateCantitate.UseVisualStyleBackColor = true;
            this.buttonUpdateCantitate.Click += new System.EventHandler(this.buttonUpdateCantitate_Click);
            // 
            // dataGridView_cart
            // 
            this.dataGridView_cart.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_cart.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_cart.BackgroundColor = System.Drawing.Color.SlateGray;
            this.dataGridView_cart.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridView_cart.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_cart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dataGridView_cart.Location = new System.Drawing.Point(3, 78);
            this.dataGridView_cart.Name = "dataGridView_cart";
            this.dataGridView_cart.Size = new System.Drawing.Size(510, 200);
            this.dataGridView_cart.TabIndex = 1;
            // 
            // label_cart
            // 
            this.label_cart.AutoSize = true;
            this.label_cart.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_cart.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.label_cart.Location = new System.Drawing.Point(83, 41);
            this.label_cart.Name = "label_cart";
            this.label_cart.Size = new System.Drawing.Size(125, 22);
            this.label_cart.TabIndex = 0;
            this.label_cart.Text = "Shopping Cart";
            this.label_cart.UseWaitCursor = true;
            // 
            // panel_center
            // 
            this.panel_center.Controls.Add(this.panel_personal);
            this.panel_center.Controls.Add(this.panel_cos);
            this.panel_center.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_center.Location = new System.Drawing.Point(142, 76);
            this.panel_center.Name = "panel_center";
            this.panel_center.Size = new System.Drawing.Size(1082, 531);
            this.panel_center.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(1379, 651);
            this.Controls.Add(this.panel_center);
            this.Controls.Add(this.panel_top);
            this.Controls.Add(this.panel_left);
            this.Controls.Add(this.panel_bottom);
            this.Controls.Add(this.panel_right);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel_left.ResumeLayout(false);
            this.panel_right.ResumeLayout(false);
            this.panel_right.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel_register.ResumeLayout(false);
            this.panel_register.PerformLayout();
            this.panel_personal.ResumeLayout(false);
            this.panel_personal.PerformLayout();
            this.panel_cos.ResumeLayout(false);
            this.panel_cos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_cart)).EndInit();
            this.panel_center.ResumeLayout(false);
            this.panel_center.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_bottom;
        private System.Windows.Forms.Panel panel_left;
        private System.Windows.Forms.Panel panel_right;
        private System.Windows.Forms.Panel panel_top;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.TextBox user;
        private System.Windows.Forms.Button button_register;
        private System.Windows.Forms.Button login;
        private System.Windows.Forms.TreeView treeViewUserMenu;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label wrong_dates;
        private System.Windows.Forms.Button button_personal;
        private System.Windows.Forms.Button button_cart;
        private System.Windows.Forms.Panel panel_register;
        private System.Windows.Forms.Label wrong_pass;
        private System.Windows.Forms.Button signup;
        private System.Windows.Forms.TextBox textBox_confirm_pass;
        private System.Windows.Forms.TextBox textBox_new_pass;
        private System.Windows.Forms.TextBox textBox_new_user;
        private System.Windows.Forms.Button button_eye;
        private System.Windows.Forms.Button button_home;
        private System.Windows.Forms.Panel panel_personal;
        private System.Windows.Forms.Button button_pers_modify;
        private System.Windows.Forms.Button button_edit_personal;
        private System.Windows.Forms.Label label_pers_fname;
        private System.Windows.Forms.Label label_mandatory;
        private System.Windows.Forms.TextBox textBox_pers_fname;
        private System.Windows.Forms.TextBox textBox_pers_lname;
        private System.Windows.Forms.Label label_pers_phone;
        private System.Windows.Forms.TextBox textBox_pers_adress;
        private System.Windows.Forms.Label label_pers_adress;
        private System.Windows.Forms.TextBox textBox_pers_phone;
        private System.Windows.Forms.Label label_pers_lname;
        private System.Windows.Forms.Panel panel_cos;
        private System.Windows.Forms.Label label_cart;
        private System.Windows.Forms.Panel panel_center;
        private System.Windows.Forms.DataGridView dataGridView_cart;
        private System.Windows.Forms.Button buttonUpdateCantitate;
        private System.Windows.Forms.Button buttonStergeProdus;
        private System.Windows.Forms.TextBox textBoxQuantity;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonPlaceOrder;
        private System.Windows.Forms.Label labelPretTotal;
    }
}
