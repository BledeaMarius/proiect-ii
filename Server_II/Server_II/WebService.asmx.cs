﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Server_II
{

    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class WebService : System.Web.Services.WebService
    {
        SqlConnection myCon = new SqlConnection();
        string connString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Marius\Documents\proiect-ii\Server_II\Server_II\App_Data\Magazin_online.mdf;Integrated Security=True";
        [WebMethod]
        public int plasareComanda(int userId)
        {
            string sql = null;
            int deleteSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "Delete from Cos where user_id=@userId";

                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@userId", userId);
                        deleteSuccesful = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (deleteSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int getProdusIdByNume(string nume)
        {
            myCon.ConnectionString = connString;
            myCon.Open();
            int produsId =  -1;
            try
            {
                SqlCommand cmd = new SqlCommand("Select produs_id from Produse where Convert(Varchar,nume)='" + nume + "'", myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    produsId = Convert.ToInt32(reader[0]);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return produsId;
        }
        [WebMethod]
        public int getIdCategByNumeCateg(string nume)
        {
            myCon.ConnectionString = connString;
            myCon.Open();
            int idCateg = -1;
            try
            {
                SqlCommand cmd = new SqlCommand("Select categorie_id from Categorii Where Convert(Varchar,nume)='" + nume + "'", myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    idCateg = Convert.ToInt32(reader[0]);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return idCateg;
        }
        [WebMethod]
        public int getIdMarcaByNumeMarca(string nume)
        {
            myCon.ConnectionString = connString;
            myCon.Open();
            int idMarca = -1;
            try
            {
                SqlCommand cmd = new SqlCommand("Select marca_id from Marci Where Convert(Varchar,nume)='" + nume + "'", myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    idMarca = Convert.ToInt32(reader[0]);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return idMarca;
        }
        [WebMethod]
        public string getNumeByCategorieId(int categId)
        {
            myCon.ConnectionString = connString;
            myCon.Open();
            string numeCateg = "";
            try
            {
                SqlCommand cmd = new SqlCommand("Select nume from Categorii Where categorie_id=" + categId, myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    numeCateg = reader[0].ToString();

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return numeCateg;
        }
        [WebMethod]
        public string getNumeByMarcaId(int marcaId)
        {
            myCon.ConnectionString = connString;
            myCon.Open();
            string numeMarca = "";
            try
            {
                SqlCommand cmd = new SqlCommand("Select nume from Marci Where marca_id="+marcaId, myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    numeMarca = reader[0].ToString();
                   
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return numeMarca;
        }
        [WebMethod]
       public List<Produse> getProduseByNume(string nume)
        {
            List<Produse> produses = new List<Produse>();
            myCon.ConnectionString = connString;
            myCon.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Select * from Produse Where Convert(Varchar,nume)='" + nume+"'", myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Produse p = new Produse()
                    {
                        produs_id = Convert.ToInt32(reader[0]),
                        categorii_id = Convert.ToInt32(reader[1]),
                        marca_id = Convert.ToInt32(reader[2]),
                        nume = reader[3].ToString(),
                        descriere =reader[4].ToString(),
                        cantitate=Convert.ToInt32(reader[5]),
                        pret=Convert.ToDouble(reader[6])

                    };
                    produses.Add(p);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return produses;
        }
        [WebMethod]
        public int StergereProdusDinCos(int produsId,int userId)
        {
            string sql = null;
            int deleteSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "Delete from Cos where user_id=@id and produs_id=@produsId";
                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@id", userId);
                        cmd.Parameters.AddWithValue("@produsId",produsId);
                        deleteSuccesful = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (deleteSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int ModificareCantitateCos(int userId, int cantitate,int produs_id)
        {
            int succes = 0;
            try
            {
                myCon.ConnectionString = connString;
                myCon.Open();
                String sql = "UPDATE Cos SET cant_cos=@cantitate where user_id=@userId and produs_id=@produs_id";
                using (SqlCommand cmd = new SqlCommand(sql, myCon))
                {
                    cmd.Parameters.AddWithValue("@cantitate", cantitate);
                    cmd.Parameters.AddWithValue("@userId", userId);
                    cmd.Parameters.AddWithValue("@produs_id", produs_id);
                    succes = cmd.ExecuteNonQuery();
                    cmd.Parameters.Clear();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return succes;
        }
        [WebMethod]
        public int ModificareCantitateProdus(int id, int cantitate)
        {
            int succes = 0;
            try
            {
                myCon.ConnectionString = connString;
                myCon.Open();
                String sql = "UPDATE Produse SET cantitate=@cantitate where produs_id=@id ";
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@cantitate", cantitate);
                        cmd.Parameters.AddWithValue("@id", id);
                        succes = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }
            }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                return succes;
        }

        [WebMethod]
        public int getCantitateProdus(int id)
        {
            int p = 0;
            myCon.ConnectionString = connString;
            myCon.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Select cantitate from Produse where produs_id=" + id, myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    p = Convert.ToInt32(reader[0]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return p;
        }
        [WebMethod]
        public Produse getProdusIdByNumeProdus(string numeProdus)
        {
            Produse p = null;
            myCon.ConnectionString = connString;
            myCon.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Select * from Produse where Convert(Varchar,nume) like '" + numeProdus+"'", myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    p = new Produse()
                    {
                        produs_id = Convert.ToInt32(reader[0]),
                        categorii_id = Convert.ToInt32(reader[1]),
                        marca_id = Convert.ToInt32(reader[2]),
                        nume = reader[3].ToString(),
                        descriere = reader[4].ToString(),
                        cantitate = Convert.ToInt32(reader[5]),
                        pret = Convert.ToDouble(reader[6]),
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return p;
        }
        [WebMethod]
        public Users getUserByEmail(string email)
        {
            Users u = null;
            myCon.ConnectionString = connString;
            myCon.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Select * from Users Where Convert(Varchar,email)= '"+ email+"'", myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    u = new Users()
                    {
                        user_id = Convert.ToInt32(reader[0]),
                        email = reader[1].ToString(),
                        pass=reader[2].ToString(),

                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return u;
        }

        [WebMethod]
        public Categorii getCategorieById(int categorie_id)
        {
            Categorii c = null;
            myCon.ConnectionString = connString;
            
            try
            {myCon.Open();
                SqlCommand cmd = new SqlCommand("Select * from Categorii Where categorie_id=" + categorie_id, myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    c = new Categorii()
                    {
                        categorie_id = Convert.ToInt32(reader[0]),
                        nume = reader[1].ToString(),
                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return c;
        }
        [WebMethod]
        public Marci getMarcaById(int marca_id)
        {
            Marci m = null;
            myCon.ConnectionString = connString;
            myCon.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Select * from Marci Where marca_id=" + marca_id, myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    m = new Marci()
                    {
                        marca_id = Convert.ToInt32(reader[0]),
                        nume = reader[1].ToString(),
                       

                    };
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return m;
        }
        [WebMethod]
        public Produse getProdusById(int produs_id)
        {
            Produse p = null;
            myCon.ConnectionString = connString;
            myCon.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Select * from Produse Where produs_id=" + produs_id, myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    p = new Produse()
                    {
                        produs_id = Convert.ToInt32(reader[0]),
                        categorii_id = Convert.ToInt32(reader[1]),
                        marca_id = Convert.ToInt32(reader[2]),
                        nume = reader[3].ToString(),
                        descriere = reader[4].ToString(),
                        cantitate = Convert.ToInt32(reader[5]),
                        pret = Convert.ToDouble(reader[6]),

                    };
              }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return p;
        }
        [WebMethod]
        public List<Cos> getCosByUserId(int user_id)
        {
            List<Cos> cos = new List<Cos>();
            Boolean found = false;
            myCon.ConnectionString = connString;
           
            try
            { myCon.Open();
                SqlCommand cmd = new SqlCommand("Select * from Cos Where user_id=" + user_id, myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Cos c= new Cos()
                    {
                        nr_comanda = Convert.ToInt32(reader[0]),
                        user_id = Convert.ToInt32(reader[1]),
                        produs_id = Convert.ToInt32(reader[2]),
                        cant_cos = Convert.ToInt32(reader[3]),
                        nr_cos = Convert.ToInt32(reader[4]),

                    };
                    cos.Add(c);
                    found = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            if (found)
                return cos;
            else return null;
        }
        [WebMethod]
        public int ModificareDatePersonale(int date_personale_id,string nume,string prenume,string adresa,string telefon)
        {
            string sql = null;
            int modifSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "UPDATE Date_Personale SET nume=@nume,prenume=@prenume,adresa=@adresa,telefon=@telefon WHERE date_personale_id=@date_personale_id;";

                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@nume", nume);
                        cmd.Parameters.AddWithValue("@prenume", prenume);
                        cmd.Parameters.AddWithValue("@adresa", adresa);
                        cmd.Parameters.AddWithValue("@telefon", telefon);
                        cmd.Parameters.AddWithValue("@date_personale_id", date_personale_id);
                        modifSuccesful = cmd.ExecuteNonQuery();

                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (modifSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public Date_Personale getInfosPerson(int id)
        {
            Date_Personale dp=null;
            myCon.ConnectionString = connString;
            myCon.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Select * from Date_Personale Where date_personale_id="+id, myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    dp = new Date_Personale()
                    {
                        date_personale_id = Convert.ToInt32(reader[0]),
                        nume = reader[1].ToString(),
                        prenume = reader[2].ToString(),
                        adresa = reader[3].ToString(),
                        telefon = reader[4].ToString()

                    };
                   

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return dp;
        }

        [WebMethod]
        public int getIndiceMaxUsers()
        {
            return getIndiceMaxim("users", "user_id");
        }
        [WebMethod]
        public int getIndiceMaxNrCos()
        {
            return getIndiceMaxim("Cos", "nr_cos");
        }
        [WebMethod]
        public int getIndiceMaxCos()
        {
            return getIndiceMaxim("Cos", "nr_comanda");
        }

        [WebMethod]
        public int getIndiceMaxDatePers()
        {
            return getIndiceMaxim("Date_Personale", "date_personale_id");
        }
        [WebMethod]
        public int getIndiceMaxCategorii()
        {
            return getIndiceMaxim("Categorii", "categorie_id");
        }
        [WebMethod]
        public int getIndiceMaxMarci()
        {
            return getIndiceMaxim("Marci", "marca_id");
        }
        [WebMethod]
        public int getIndiceMaxProduse()
        {
            return getIndiceMaxim("Produse", "produs_id");
        }
        
        public int getIndiceMaxim(string nume_tabela, string nume_coloana)
        {
            myCon.ConnectionString = connString;
            myCon.Open();
            int maxIndice = -1;
            try
            {
                SqlCommand cmd = new SqlCommand("Select max("+nume_coloana.ToString()+") from "+nume_tabela.ToString(), myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    maxIndice =Convert.ToInt32( reader[0]);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return maxIndice;
        }

        [WebMethod]
        public int ModificareProdus(int produs_id, int categorii_id, int marca_id, string nume, string descriere,int cantitate,double pret)
        {
            string sql = null;
            int modifSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "UPDATE Produse SET categorii_id=@categorii_id,marca_id=@marca_id,nume=@nume,descriere=@descriere,cantitate=@cantitate,pret=@pret WHERE produs_id=@produs_id;";

                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@categorii_id", categorii_id);
                        cmd.Parameters.AddWithValue("@marca_id", marca_id);
                        cmd.Parameters.AddWithValue("@nume", nume);
                        cmd.Parameters.AddWithValue("@descriere", descriere);
                        cmd.Parameters.AddWithValue("@cantitate", cantitate);
                        cmd.Parameters.AddWithValue("@pret", pret);
                        cmd.Parameters.AddWithValue("@produs_id", produs_id);
                        modifSuccesful = cmd.ExecuteNonQuery();

                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (modifSuccesful > 0)
                return 1;
            else return 0;
        }
      
        [WebMethod]
        public int ModificareCos(int nr_comanda, int user_id, int produs_id, int cant_cos,int nr_cos)
        {
            string sql = null;
            int updateSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "UPDATE Cos SET user_id=@user_id, produs_id=@produs_id,cant_cos=@cant_cos,nr_cos=@nr_cos WHERE nr_comanda=@nr_comanda;";

                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@user_id", user_id);
                        cmd.Parameters.AddWithValue("@produs_id", produs_id);
                        cmd.Parameters.AddWithValue("@cant_cos", cant_cos);
                        cmd.Parameters.AddWithValue("@nr_comanda", nr_comanda);
                        cmd.Parameters.AddWithValue("@nr_cos", nr_cos);
                        updateSuccesful = cmd.ExecuteNonQuery();

                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (updateSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int InsertUser(int user_id,string email, string pass ,int isAdmin)
        {
            string sql = null;
            int insertSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "insert into Users ([user_id],[email],[pass],[is_admin]) values(@user_id,@email,@pass,@isAdmin)";
                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@user_id", user_id);
                        cmd.Parameters.AddWithValue("@email", email);
                        cmd.Parameters.AddWithValue("@pass", pass);
                        cmd.Parameters.AddWithValue("@isAdmin", isAdmin);
                        insertSuccesful = cmd.ExecuteNonQuery();

                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (insertSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int StergereUser(int id)
        {
            string sql = null;
            int deleteSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "Delete from Users where user_id=@id";

                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        deleteSuccesful = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (deleteSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int InsertProduse(int produsId, int categoriiId, int marcaId , string nume,string descriere,int cantitate,double pret)
        {
            string sql = null;
            int insertSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "insert into Produse ([produs_id],[categorii_id],[marca_id],[nume],[descriere],[cantitate],[pret]) values(@produsId,@categoriiID,@marcaId,@nume,@descriere,@cantitate,@pret)";
                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@produsId", produsId);
                        cmd.Parameters.AddWithValue("@categoriiId", categoriiId);
                        cmd.Parameters.AddWithValue("@marcaId", marcaId);
                        cmd.Parameters.AddWithValue("@nume", nume);
                        cmd.Parameters.AddWithValue("@descriere", descriere);
                        cmd.Parameters.AddWithValue("@cantitate", cantitate);
                        cmd.Parameters.AddWithValue("@pret", pret);
                        insertSuccesful = cmd.ExecuteNonQuery();

                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (insertSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int StergereProdus(int idProd)
        {
            string sql = null;
            int deleteSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "Delete from Produse where produs_id=@id";

                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@id", idProd);
                        deleteSuccesful = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (deleteSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int InsertMarci(int marca_id, string nume)
        {
            string sql = null;
            int insertSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "insert into Marci ([marca_id],[nume]) values(@marcaId,@nume)";
                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@marcaId", marca_id);
                        cmd.Parameters.AddWithValue("@nume", nume);
                        insertSuccesful = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (insertSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int StergereMarca(int marcaId)
        {
            string sql = null;
            int deleteSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "Delete from Marci where marca_id=@id";

                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@id", marcaId);
                        deleteSuccesful = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (deleteSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int InsertDate_Personale(int date_personale_id, string name, string prenume, string adresa, string telefon)
        {
            string sql = null;
            int insertSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "insert into Date_Personale ([date_personale_id],[nume],[prenume],[adresa],[telefon]) values(@date_personaleId,@nume,@prenume,@adresa,@telefon)";
                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@date_personaleId", date_personale_id);
                        cmd.Parameters.AddWithValue("@nume", name);
                        cmd.Parameters.AddWithValue("@prenume", prenume);
                        cmd.Parameters.AddWithValue("@adresa", adresa);
                        cmd.Parameters.AddWithValue("@telefon", telefon);
                        insertSuccesful = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                        
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (insertSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int StergereDatePers(int datePersId)
        {
            string sql = null;
            int deleteSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "Delete from Date_Personale where date_personale_id=@id";

                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@id", datePersId);
                        deleteSuccesful = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (deleteSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int InsertCategorii(int categId, string nume)
        {
            string sql = null;
            int insertSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "insert into Categorii ([categorie_id],[nume]) values(@id,@nume)";
                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@id", categId);
                        cmd.Parameters.AddWithValue("@nume", nume);

                        insertSuccesful = cmd.ExecuteNonQuery();

                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (insertSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int StergereCategorii(int categoriiId)
        {
            string sql = null;
            int deleteSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "Delete from Categorii where categorie_id=@id";

                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@id", categoriiId );
                        deleteSuccesful = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (deleteSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int InsertCos(int nr_comanda,int user_id,int produs_id, int cant_cos,int nr_cos)
        {
            string sql = null;
            int insertSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "insert into Cos ([nr_comanda],[user_id],[produs_id],[cant_cos],[nr_cos]) values(@nrCom,@userId,@prodId,@cantitate,@nr_cos)";
                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@nrCom", nr_comanda);
                        cmd.Parameters.AddWithValue("@userId", user_id);
                        cmd.Parameters.AddWithValue("@prodId", produs_id);
                        cmd.Parameters.AddWithValue("@cantitate", cant_cos);
                        cmd.Parameters.AddWithValue("@nr_cos", nr_cos);
                        insertSuccesful = cmd.ExecuteNonQuery();

                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (insertSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public int StergereCos(int id)
        {
            string sql = null;
            int deleteSuccesful = 0;
            using (myCon = new SqlConnection(connString))
            {
                sql = "Delete from Cos where user_id=@id";

                try
                {
                    myCon.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, myCon))
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                        deleteSuccesful = cmd.ExecuteNonQuery();
                        cmd.Parameters.Clear();
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            if (deleteSuccesful > 0)
                return 1;
            else return 0;
        }
        [WebMethod]
        public List<Users> getUsers()
        {
            List<Users> users = new List<Users>();
            myCon.ConnectionString = connString;
            myCon.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Select * from Users", myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Users u = new Users()
                    {
                        user_id = Convert.ToInt32(reader[0]),
                        email = Convert.ToString(reader[1]),
                        pass = Convert.ToString(reader[2]),
                        is_admin = Convert.ToInt32(reader[3])
                    };

                    users.Add(u);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return users;

        }
        [WebMethod]
        public List<Produse> getProduse()
        {
            List<Produse> produse = new List<Produse>();
            myCon.ConnectionString = connString;
            
            try
            {myCon.Open();
                SqlCommand cmd = new SqlCommand("Select * from Produse", myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Produse p = new Produse()
                    {
                        produs_id = Convert.ToInt32(reader[0]),
                        categorii_id = Convert.ToInt32(reader[1]),
                        marca_id=Convert.ToInt32(reader[2]),
                        nume=Convert.ToString(reader[3]),
                        descriere=Convert.ToString(reader[4]),
                        cantitate=Convert.ToInt32(reader[5]),
                        pret=Convert.ToDouble(reader[6])
                    };

                    produse.Add(p);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return produse;

        }
        [WebMethod]
        public List<Marci> getMarci()
        {
            List<Marci> marci = new List<Marci>();
            myCon.ConnectionString = connString;
            myCon.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Select * from Marci", myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Marci m = new Marci()
                    {
                        marca_id = Convert.ToInt32(reader[0]),
                        nume = Convert.ToString(reader[1])
                    };

                    marci.Add(m);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return marci;

        }
        [WebMethod]
        public List<Categorii> getCategorii()
        {
            List<Categorii> categorii = new List<Categorii>();
            myCon.ConnectionString = connString;
            myCon.Open();

            try
            {
                SqlCommand cmd = new SqlCommand("Select * from Categorii", myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Categorii c = new Categorii()
                    {
                        categorie_id = Convert.ToInt32(reader[0]),
                        nume = Convert.ToString(reader[1])
                    };

                    categorii.Add(c);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return categorii;

        }
        [WebMethod]
        public List<Cos> getCos()
        {
            List<Cos> cos = new List<Cos>();
            myCon.ConnectionString = connString;
            myCon.Open();

            SqlDataAdapter daCateg = new SqlDataAdapter("Select * from Cos", myCon);
            try
            {
                SqlCommand cmd = new SqlCommand("Select * from Cos", myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Cos c = new Cos()
                    {
                        nr_comanda= Convert.ToInt32(reader[0]),
                        user_id = Convert.ToInt32(reader[1]),
                        produs_id=Convert.ToInt32(reader[2]),
                        cant_cos=Convert.ToInt32(reader[3]),
                        nr_cos=Convert.ToInt32(reader[4])
                    };

                    cos.Add(c);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return cos;

        }

        [WebMethod]
        public List<Date_Personale> GetDate_Personale()
        {
            List<Date_Personale> date_Personale = new List<Date_Personale>();
            myCon.ConnectionString = connString;
            myCon.Open();
            try
            {
                SqlCommand cmd = new SqlCommand("Select * from Date_Personale", myCon);
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Date_Personale dp = new Date_Personale()
                    {
                        date_personale_id = Convert.ToInt32(reader[0]),
                        nume = Convert.ToString(reader[1]),
                        prenume=Convert.ToString(reader[2]),
                        adresa=Convert.ToString(reader[3]),
                        telefon=Convert.ToString(reader[4])
                    };

                    date_Personale.Add(dp);

                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return date_Personale;

        }

    }
}
