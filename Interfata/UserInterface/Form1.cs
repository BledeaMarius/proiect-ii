﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace UserInterface
{
    public partial class Form1 : Form
    {
        static DataBaseServiceReference.WebServiceSoapClient service = new DataBaseServiceReference.WebServiceSoapClient();
        DataBaseServiceReference.Categorii[] categorii = service.getCategorii();
        DataBaseServiceReference.Marci[] marci = service.getMarci();
        DataBaseServiceReference.Users[] users = service.getUsers();
        DataBaseServiceReference.Date_Personale date_personale = null;
        DataBaseServiceReference.Cos[] cos = service.getCos();
        DataBaseServiceReference.Produse produs = null;
        FormAdmin formAdmin;
        

        static int id_current_user;
        static int nr_cos_current_user;
        public Form1()
        {
            InitializeComponent();
            WindowState = FormWindowState.Maximized;

            panel_center.AutoScroll = true;
            panel_center.SetAutoScrollMargin(0, 0);
            try
            {
                treeViewUserMenu.AfterSelect += (sender, eve) =>
                    {
                        TreeView menuView = (TreeView)sender;
                        if (menuView.SelectedNode.Parent != null)
                        {
                            DataBaseServiceReference.Produse[] produseShow = service.getProduse();
                            populate_Products(produseShow, int.Parse(menuView.SelectedNode.Parent.Name), int.Parse(menuView.SelectedNode.Name));
                        }

                    };
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            foreach (DataBaseServiceReference.Categorii item in categorii)
            {

                try
                {
                    TreeNode treeItem = new TreeNode();
                    treeItem.Name = item.categorie_id.ToString();
                    treeItem.Text = item.nume;
                    foreach (DataBaseServiceReference.Marci child in marci)
                    {
                        TreeNode treeItemChild = new TreeNode();
                        treeItemChild.Name = child.marca_id.ToString();
                        treeItemChild.Text = child.nume;
                        treeItem.Nodes.Add(treeItemChild);
                    }
                    treeViewUserMenu.Nodes.Add(treeItem);
                }
                catch (Exception exc)
                {
                    System.Windows.Forms.MessageBox.Show(exc.ToString());
                }
            }
        }
        public static string removeLastChar(string text)
        {
            String a = null ;
            try
            {
                 a = text;
                a = a.Remove(a.Length - 1);
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return a;
        }

        private void populate_Products(DataBaseServiceReference.Produse[] produse, int categorie_id, int marca_id)
        {
            try
            {
                panel_personal.SendToBack();
                panel_personal.Visible = false;
                panel_cos.SendToBack();
                panel_cos.Visible = false;
                List<DataBaseServiceReference.Produse> p = new List<DataBaseServiceReference.Produse>();
                bool k = true;
                while (k)
                {
                    k = false;
                    foreach (Control ctrl in panel_center.Controls)
                    {
                        if (ctrl.GetType().ToString() == "System.Windows.Forms.FlowLayoutPanel")
                        {
                            k = true;
                            panel_center.Controls.Remove(ctrl);
                        };

                    }
                }
                foreach (DataBaseServiceReference.Produse produs in produse)
                {
                    if (produs.categorii_id == categorie_id && produs.marca_id == marca_id)
                    {
                        p.Add(produs);
                    }
                }

                for (int idx = 0; idx < p.Count(); idx++)
                {

                    DataBaseServiceReference.Produse pr = p[idx];

                    FlowLayoutPanel flowLay = new FlowLayoutPanel();
                    flowLay.FlowDirection = FlowDirection.TopDown;
                    flowLay.Size = new Size(290, 290);
                    flowLay.Location = new Point(300 * (idx % 3), 300 * (idx / 3));

                    Label prTextBox = new Label();

                    prTextBox.Text = removeLastChar(pr.nume);
                    prTextBox.Enabled = false;
                    prTextBox.Size = new Size(250, 30);
                    prTextBox.Font = new Font("Microsoft Sans Serif", 11, FontStyle.Bold);

                    Button addCart = new Button();
                    addCart.Text = "Adauga in cos";
                    addCart.Size = new Size(250, 30);
                    addCart.Font = new Font("Microsoft Sans Serif", 8, FontStyle.Bold);
                    addCart.Click += (object sender, EventArgs eve) => // inserare in cos
                    {
                        int stocProdus = service.getCantitateProdus(pr.produs_id);
                        if (stocProdus > 0) // daca avem pe stoc suficiente produse
                     {
                            if (id_current_user != 0)
                            {
                                Boolean productFound = false;
                                int cantitateProdus = -1;
                                DataBaseServiceReference.Cos[] cosUser = service.getCosByUserId(id_current_user);
                                if (cosUser != null)
                                {
                                    foreach (DataBaseServiceReference.Cos c in cosUser)
                                    {
                                        if (c.produs_id == pr.produs_id)//ce e in cos == ce adaug
                                     {
                                            productFound = true;
                                            cantitateProdus = c.cant_cos;
                                        }

                                    }
                                }
                                if (productFound)
                                {
                                    if (service.ModificareCantitateCos(id_current_user, cantitateProdus + 1, pr.produs_id) == 1)
                                    {
                                        MessageBox.Show("cantitate modif succes");
                                        service.ModificareCantitateProdus(pr.produs_id, stocProdus - 1);
                                    }
                                    else
                                        MessageBox.Show("EROARE");
                                }
                                else
                                {
                                    int nr_comanda = service.getIndiceMaxCos() + 1;
                                    if (service.InsertCos(nr_comanda, id_current_user, pr.produs_id, 1, nr_cos_current_user) == 1)
                                    {
                                        service.ModificareCantitateProdus(pr.produs_id, stocProdus - 1);
                                        MessageBox.Show("Product added succesfully");
                                    }
                                    else
                                    {
                                        MessageBox.Show("Error inserting product");
                                    }
                                }

                            }
                            else
                            {
                                MessageBox.Show("Please register or login!");
                            }
                        }
                        else
                        { MessageBox.Show("Cantitate Insuficienta"); }
                    };

                    Label prTextBoxDesc = new Label();
                    prTextBoxDesc.Text = pr.descriere;
                    prTextBoxDesc.Enabled = false;
                    prTextBoxDesc.Size = new Size(250, 30);
                    prTextBoxDesc.Font = new Font("Microsoft Sans Serif", 8, FontStyle.Italic);

                    PictureBox prImg = new PictureBox();
                    string categorie = service.getNumeByCategorieId(pr.categorii_id);
                    string marca = service.getNumeByMarcaId(pr.marca_id);
                    //prImg.ImageLocation= "..\\..\\media\\home.png";
                    string imagePath = string.Format("..\\..\\media\\{0}_{1}_{2}.png", categorie, marca, pr.nume);
                    prImg.ImageLocation = imagePath;
                    prImg.Size = new Size(150, 160);
                    prImg.SizeMode = PictureBoxSizeMode.StretchImage;

                    Label price = new Label();
                    price.Text = "Pret:" + pr.pret.ToString();
                    price.Enabled = false;
                    price.Size = new Size(95, 15);
                    price.Font = new Font("Microsoft Sans Serif", 10);


                    flowLay.Controls.Add(prTextBox);
                    flowLay.Controls.Add(prTextBoxDesc);
                    flowLay.Controls.Add(price);
                    flowLay.Controls.Add(addCart);
                    flowLay.Controls.Add(prImg);

                    panel_center.Controls.Add(flowLay);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void quit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void user_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                user.Text = "";
                user.Font = new Font("Microsoft Sans Serif", 8, FontStyle.Regular);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void password_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                password.Text = "";
                password.UseSystemPasswordChar = true;
                password.Font = new Font("Microsoft Sans Serif", 8, FontStyle.Regular);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void login_Click(object sender, EventArgs e)
        {
            try
            {
                users = service.getUsers();
                Boolean found = false;
                foreach (DataBaseServiceReference.Users u in users)
                {
                    if (user.Text.Equals(u.email) && password.Text.Equals(u.pass))
                    {
                        found = true;
                        id_current_user = u.user_id;
                        DataBaseServiceReference.Cos[] produse_user = service.getCosByUserId(id_current_user);
                        if (produse_user != null)// daca exista produse in cos
                            nr_cos_current_user = produse_user[0].nr_cos;
                        else
                            nr_cos_current_user = service.getIndiceMaxNrCos() + 1;
                        if (u.is_admin == 0)
                        {
                            this.panel_right.SendToBack();
                            this.panel1.BringToFront();
                            this.panel1.Visible = true;

                        }
                        else
                        {
                            formAdmin = new FormAdmin();
                            formAdmin.Show();
                            this.Hide();

                        }


                    }
                }
                if (!found)
                {
                    wrong_dates.Text = "Username or password" + Environment.NewLine + " is incorrect!";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox_new_user_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                textBox_new_user.Text = "";
                textBox_new_user.Font = new Font("Microsoft Sans Serif", 8, FontStyle.Regular);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox_new_pass_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                textBox_new_pass.Text = "";
                textBox_new_pass.UseSystemPasswordChar = true;
                textBox_new_pass.Font = new Font("Microsoft Sans Serif", 8, FontStyle.Regular);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBox_confirm_pass_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                textBox_confirm_pass.Text = "";
                textBox_confirm_pass.UseSystemPasswordChar = true;
                textBox_confirm_pass.Font = new Font("Microsoft Sans Serif", 8, FontStyle.Regular);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button_eye_MouseDown(object sender, MouseEventArgs e)
        {
            try
            {
                textBox_new_pass.UseSystemPasswordChar = false;
                textBox_confirm_pass.UseSystemPasswordChar = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button_eye_MouseUp(object sender, MouseEventArgs e)
        {
            try
            {
                textBox_new_pass.UseSystemPasswordChar = true;
                textBox_confirm_pass.UseSystemPasswordChar = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void signup_Click(object sender, EventArgs e)
        {
            try {
                users = service.getUsers();
                Boolean emailfound = false;
                if (textBox_new_pass.Text.Equals(textBox_confirm_pass.Text))
                {
                    foreach (DataBaseServiceReference.Users u in users)
                    {
                        if (textBox_new_user.Text.Equals(u.email))
                        {
                            emailfound = true;
                            wrong_pass.Text = "E-mail already existing";
                        }
                    }
                    if (!emailfound)
                    {
                        if (service.InsertUser(service.getIndiceMaxUsers() + 1, textBox_new_user.Text, textBox_new_pass.Text, 0) == 1)
                        {
                            wrong_pass.Text = "User registered";
                            Task.Delay(5000);
                            panel_register.Visible = false;
                            panel_register.SendToBack();
                        }
                    }
                }
                else
                    wrong_pass.Text = "Passwords don't match!";
            }
        catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
}

        private void button_register_Click(object sender, EventArgs e)
        {
            try
            {
                panel_register.BringToFront();
                panel_register.Visible = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button_personal_Click(object sender, EventArgs e)
        {
            try
            {
                panel_personal.Location = new Point(6, 6);
                panel_personal.Size = new Size(800, 500);
                panel_personal.BringToFront();
                panel_personal.Visible = true;
                panel_cos.SendToBack();
                panel_cos.Visible = false;
                panel_personal.Size = new Size(1200,800);
                panel_personal.Location = new Point(0, 0);

                date_personale = service.getInfosPerson(id_current_user);

                if (date_personale != null)
                {
                    textBox_pers_fname.Text = date_personale.prenume.ToString();
                    textBox_pers_lname.Text = date_personale.nume.ToString();
                    textBox_pers_adress.Text = date_personale.adresa.ToString();
                    textBox_pers_phone.Text = date_personale.telefon.ToString();
                }
                else
                {
                    label_mandatory.Text = "All personal details marked with * are mandatory.";
                    label_pers_fname.Text = "First Name*";
                    label_pers_lname.Text = "Last Name*";
                    label_pers_adress.Text = "Address*";
                    label_pers_phone.Text = "Phone*";
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button_edit_personal_Click(object sender, EventArgs e)
        {
            try
            {
                textBox_pers_fname.ReadOnly = false;
                textBox_pers_lname.ReadOnly = false;
                textBox_pers_adress.ReadOnly = false;
                textBox_pers_phone.ReadOnly = false;
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button_pers_modify_Click(object sender, EventArgs e)
        {
            try
            {
                if (date_personale == null)
                {
                    if (service.InsertDate_Personale(id_current_user, textBox_pers_lname.Text.ToString(), textBox_pers_fname.Text.ToString(), textBox_pers_adress.Text.ToString(), textBox_pers_phone.Text.ToString()) == 1)
                        label_mandatory.Text = "Personal details inserted successfully.";
                    else
                        label_mandatory.Text = "Error inserting details.";

                }
                else
                {
                    if (service.ModificareDatePersonale(id_current_user, textBox_pers_lname.Text.ToString(), textBox_pers_fname.Text.ToString(), textBox_pers_adress.Text.ToString(), textBox_pers_phone.Text.ToString()) == 1)
                        label_mandatory.Text = "Personal details updated successfully.";
                    else
                        label_mandatory.Text = "Error updating details.";
                }
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button_home_Click(object sender, EventArgs e)
        {
            try
            {
                textBox_pers_fname.ReadOnly = true;
                textBox_pers_lname.ReadOnly = true;
                textBox_pers_adress.ReadOnly = true;
                textBox_pers_phone.ReadOnly = true;
                panel_personal.SendToBack();
                panel_personal.Visible = false;
                panel_cos.SendToBack();
                panel_cos.Visible = false;
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        private void button_cart_Click(object sender, EventArgs e)
        {
            try
            {
                panel_cos.Location = new Point(3, 3);
                panel_personal.SendToBack();
                panel_personal.Visible = false;
                panel_cos.BringToFront();
                panel_cos.Visible = true;
                panel_cos.Location = new Point(0, 0);
                panel_cos.Size = new Size(1200, 800);
                refreshCos();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void refreshCos()
        {
            double suma = 0;
            try
            {
                List<Elemente_cos> elemente_Cos = new List<Elemente_cos>();
                cos = service.getCosByUserId(id_current_user);
                if (cos == null)
                { dataGridView_cart.Enabled = false;
                    dataGridView_cart.Visible = false;
                }
                else
                {
                    dataGridView_cart.Enabled = true;
                    foreach (DataBaseServiceReference.Cos c in cos)
                    {
                        Elemente_cos ec = new Elemente_cos();
                        produs = service.getProdusById(c.produs_id);
                        ec.NumeProdus = produs.nume;
                        ec.Marca = service.getMarcaById(produs.marca_id).nume;
                        ec.Categorie = service.getCategorieById(produs.categorii_id).nume;
                        ec.Cantitate = c.cant_cos;
                        ec.Pret = c.cant_cos * produs.pret;
                        elemente_Cos.Add(ec);
                        suma += ec.Pret;

                    }
                    dataGridView_cart.DataSource = elemente_Cos;
                    dataGridView_cart.Visible = true;
                    if (suma == 0)
                    {
                        labelPretTotal.Text = "Pret total: -";
                    }
                    else
                        labelPretTotal.Text = "Pret total: " + suma;
                }
            }catch(Exception ex)
            { MessageBox.Show("Refresh cos "+ex.Message); }
        }

        private void buttonUpdateCantitate_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow dgv = dataGridView_cart.CurrentRow;
                int quantity = Convert.ToInt32(dgv.Cells[3].Value);
                DataBaseServiceReference.Produse p = service.getProdusIdByNumeProdus( dgv.Cells[0].Value.ToString());
                int q = 0;
                q = p.cantitate - Convert.ToInt32(textBoxQuantity.Text) + quantity;
                if(q >= 0)
                {
                    MessageBox.Show( p.produs_id+" "+p.nume );

                    if ( !(service.ModificareCantitateCos(id_current_user, Convert.ToInt32(textBoxQuantity.Text), p.produs_id ) == 1) )
                        MessageBox.Show("Quantity from basket not updated");

                    if (! (service.ModificareCantitateProdus(p.produs_id, q) == 1))
                        MessageBox.Show("Quantity not updated in Produse");

                    MessageBox.Show("Quantity updated successfully");
                    refreshCos();
                }
                else
                    MessageBox.Show("Quantity not enough");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonStergeProdus_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow dgv = dataGridView_cart.CurrentRow;
                DataBaseServiceReference.Produse produs= service.getProdusIdByNumeProdus(dgv.Cells[0].Value.ToString());
            
                if (service.StergereProdusDinCos(produs.produs_id,id_current_user) == 1)
                {
                    MessageBox.Show("Product deleted successfully");
                    refreshCos();
                }
                else
                    MessageBox.Show("Error while deleting product");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonPlaceOrder_Click(object sender, EventArgs e)
        {
            try
            {
                if (service.plasareComanda(id_current_user) == 1)
                {
                    refreshCos();
                    MessageBox.Show("Order placed successfully!");
                    
                }
                else
                    MessageBox.Show("Error while placing orders");
                
                   
            } catch(Exception ex)
            { MessageBox.Show(ex.Message); }
        }
    }
}
