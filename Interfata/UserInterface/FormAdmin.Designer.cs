﻿namespace UserInterface
{
    partial class FormAdmin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormAdmin));
            this.searchUser = new System.Windows.Forms.Button();
            this.dataGridViewAdmin = new System.Windows.Forms.DataGridView();
            this.labelUsername = new System.Windows.Forms.Label();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.panel_right = new System.Windows.Forms.Panel();
            this.wrong_dates = new System.Windows.Forms.Label();
            this.quit = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelCategMarci = new System.Windows.Forms.Panel();
            this.buttonAdaugareMarca = new System.Windows.Forms.Button();
            this.buttonAdaugareCateg = new System.Windows.Forms.Button();
            this.textBoxAdaugCateg = new System.Windows.Forms.TextBox();
            this.textBoxAdaugMarca = new System.Windows.Forms.TextBox();
            this.buttonStergeCateg = new System.Windows.Forms.Button();
            this.buttonStergeMarca = new System.Windows.Forms.Button();
            this.buttonCateg = new System.Windows.Forms.Button();
            this.dataGridViewCateg = new System.Windows.Forms.DataGridView();
            this.buttonMarci = new System.Windows.Forms.Button();
            this.dataGridViewMarci = new System.Windows.Forms.DataGridView();
            this.panelProducts = new System.Windows.Forms.Panel();
            this.textBoxNumeMarca = new System.Windows.Forms.TextBox();
            this.textBoxNumeCategorie = new System.Windows.Forms.TextBox();
            this.textBoxPretProd = new System.Windows.Forms.TextBox();
            this.textBoxCantitateProd = new System.Windows.Forms.TextBox();
            this.textBoxDescriereProd = new System.Windows.Forms.TextBox();
            this.textBoxNumeProd = new System.Windows.Forms.TextBox();
            this.buttonInsert = new System.Windows.Forms.Button();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonUpdateProducts = new System.Windows.Forms.Button();
            this.dataGridViewProduse = new System.Windows.Forms.DataGridView();
            this.buttonSearchProduct = new System.Windows.Forms.Button();
            this.textBoxSearchProduct = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panelUserDetails = new System.Windows.Forms.Panel();
            this.button_delete_order = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label_quantity = new System.Windows.Forms.Label();
            this.textBox_quantity = new System.Windows.Forms.TextBox();
            this.button_stergere = new System.Windows.Forms.Button();
            this.buttonQuantity = new System.Windows.Forms.Button();
            this.label_cantitate = new System.Windows.Forms.Label();
            this.label_search_error = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionareComenziUserToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionareProduseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.marciCategoriiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdmin)).BeginInit();
            this.panel_right.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelCategMarci.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCateg)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMarci)).BeginInit();
            this.panelProducts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProduse)).BeginInit();
            this.panelUserDetails.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // searchUser
            // 
            this.searchUser.BackColor = System.Drawing.Color.SlateGray;
            this.searchUser.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("searchUser.BackgroundImage")));
            this.searchUser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.searchUser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.searchUser.FlatAppearance.BorderSize = 0;
            this.searchUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.searchUser.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.searchUser.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.searchUser.Location = new System.Drawing.Point(355, 5);
            this.searchUser.Name = "searchUser";
            this.searchUser.Size = new System.Drawing.Size(137, 27);
            this.searchUser.TabIndex = 0;
            this.searchUser.Text = "Search";
            this.searchUser.UseVisualStyleBackColor = false;
            this.searchUser.Click += new System.EventHandler(this.searchUser_Click);
            // 
            // dataGridViewAdmin
            // 
            this.dataGridViewAdmin.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewAdmin.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewAdmin.BackgroundColor = System.Drawing.Color.SlateGray;
            this.dataGridViewAdmin.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewAdmin.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewAdmin.Location = new System.Drawing.Point(9, 49);
            this.dataGridViewAdmin.MultiSelect = false;
            this.dataGridViewAdmin.Name = "dataGridViewAdmin";
            this.dataGridViewAdmin.ReadOnly = true;
            this.dataGridViewAdmin.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewAdmin.Size = new System.Drawing.Size(465, 204);
            this.dataGridViewAdmin.TabIndex = 1;
            // 
            // labelUsername
            // 
            this.labelUsername.AutoSize = true;
            this.labelUsername.Font = new System.Drawing.Font("Cooper Black", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.labelUsername.Location = new System.Drawing.Point(5, 8);
            this.labelUsername.Name = "labelUsername";
            this.labelUsername.Size = new System.Drawing.Size(182, 19);
            this.labelUsername.TabIndex = 3;
            this.labelUsername.Text = "Search for username:";
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.BackColor = System.Drawing.Color.SlateGray;
            this.textBoxUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.textBoxUsername.Location = new System.Drawing.Point(193, 9);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(153, 20);
            this.textBoxUsername.TabIndex = 4;
            this.textBoxUsername.TextChanged += new System.EventHandler(this.textBoxUsername_TextChanged);
            // 
            // panel_right
            // 
            this.panel_right.BackColor = System.Drawing.Color.SlateGray;
            this.panel_right.Controls.Add(this.wrong_dates);
            this.panel_right.Controls.Add(this.quit);
            this.panel_right.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel_right.Location = new System.Drawing.Point(1206, 0);
            this.panel_right.Name = "panel_right";
            this.panel_right.Size = new System.Drawing.Size(149, 614);
            this.panel_right.TabIndex = 5;
            // 
            // wrong_dates
            // 
            this.wrong_dates.AutoEllipsis = true;
            this.wrong_dates.AutoSize = true;
            this.wrong_dates.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.wrong_dates.Location = new System.Drawing.Point(17, 137);
            this.wrong_dates.Name = "wrong_dates";
            this.wrong_dates.Size = new System.Drawing.Size(0, 13);
            this.wrong_dates.TabIndex = 5;
            this.wrong_dates.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // quit
            // 
            this.quit.BackColor = System.Drawing.Color.SlateGray;
            this.quit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.quit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.quit.FlatAppearance.BorderSize = 0;
            this.quit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.quit.Image = ((System.Drawing.Image)(resources.GetObject("quit.Image")));
            this.quit.Location = new System.Drawing.Point(119, 12);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(18, 18);
            this.quit.TabIndex = 0;
            this.quit.UseVisualStyleBackColor = false;
            this.quit.Click += new System.EventHandler(this.quit_Click_1);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panelCategMarci);
            this.panel1.Controls.Add(this.panelProducts);
            this.panel1.Controls.Add(this.panelUserDetails);
            this.panel1.Controls.Add(this.label_cantitate);
            this.panel1.Controls.Add(this.label_search_error);
            this.panel1.Controls.Add(this.menuStrip1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1355, 614);
            this.panel1.TabIndex = 6;
            // 
            // panelCategMarci
            // 
            this.panelCategMarci.BackColor = System.Drawing.Color.SlateGray;
            this.panelCategMarci.Controls.Add(this.buttonAdaugareMarca);
            this.panelCategMarci.Controls.Add(this.buttonAdaugareCateg);
            this.panelCategMarci.Controls.Add(this.textBoxAdaugCateg);
            this.panelCategMarci.Controls.Add(this.textBoxAdaugMarca);
            this.panelCategMarci.Controls.Add(this.buttonStergeCateg);
            this.panelCategMarci.Controls.Add(this.buttonStergeMarca);
            this.panelCategMarci.Controls.Add(this.buttonCateg);
            this.panelCategMarci.Controls.Add(this.dataGridViewCateg);
            this.panelCategMarci.Controls.Add(this.buttonMarci);
            this.panelCategMarci.Controls.Add(this.dataGridViewMarci);
            this.panelCategMarci.Location = new System.Drawing.Point(8, 422);
            this.panelCategMarci.Name = "panelCategMarci";
            this.panelCategMarci.Size = new System.Drawing.Size(811, 588);
            this.panelCategMarci.TabIndex = 7;
            this.panelCategMarci.Visible = false;
            // 
            // buttonAdaugareMarca
            // 
            this.buttonAdaugareMarca.BackColor = System.Drawing.Color.SlateGray;
            this.buttonAdaugareMarca.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonAdaugareMarca.BackgroundImage")));
            this.buttonAdaugareMarca.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonAdaugareMarca.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAdaugareMarca.FlatAppearance.BorderSize = 0;
            this.buttonAdaugareMarca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdaugareMarca.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdaugareMarca.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonAdaugareMarca.Location = new System.Drawing.Point(491, 110);
            this.buttonAdaugareMarca.Name = "buttonAdaugareMarca";
            this.buttonAdaugareMarca.Size = new System.Drawing.Size(210, 36);
            this.buttonAdaugareMarca.TabIndex = 16;
            this.buttonAdaugareMarca.Text = "Adaugare Marca";
            this.buttonAdaugareMarca.UseVisualStyleBackColor = false;
            this.buttonAdaugareMarca.Click += new System.EventHandler(this.buttonAdaugareMarca_Click);
            // 
            // buttonAdaugareCateg
            // 
            this.buttonAdaugareCateg.BackColor = System.Drawing.Color.SlateGray;
            this.buttonAdaugareCateg.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonAdaugareCateg.BackgroundImage")));
            this.buttonAdaugareCateg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonAdaugareCateg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAdaugareCateg.FlatAppearance.BorderSize = 0;
            this.buttonAdaugareCateg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonAdaugareCateg.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAdaugareCateg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonAdaugareCateg.Location = new System.Drawing.Point(491, 268);
            this.buttonAdaugareCateg.Name = "buttonAdaugareCateg";
            this.buttonAdaugareCateg.Size = new System.Drawing.Size(179, 36);
            this.buttonAdaugareCateg.TabIndex = 15;
            this.buttonAdaugareCateg.Text = "Add Category";
            this.buttonAdaugareCateg.UseVisualStyleBackColor = false;
            this.buttonAdaugareCateg.Click += new System.EventHandler(this.buttonAdaugareCateg_Click);
            // 
            // textBoxAdaugCateg
            // 
            this.textBoxAdaugCateg.BackColor = System.Drawing.Color.SlateGray;
            this.textBoxAdaugCateg.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAdaugCateg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.textBoxAdaugCateg.Location = new System.Drawing.Point(491, 211);
            this.textBoxAdaugCateg.Name = "textBoxAdaugCateg";
            this.textBoxAdaugCateg.Size = new System.Drawing.Size(155, 25);
            this.textBoxAdaugCateg.TabIndex = 14;
            this.textBoxAdaugCateg.Text = "Introduceti numele categoriei";
            this.textBoxAdaugCateg.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxAdaugCateg.Enter += new System.EventHandler(this.textBoxAdaugCateg_Enter);
            // 
            // textBoxAdaugMarca
            // 
            this.textBoxAdaugMarca.BackColor = System.Drawing.Color.SlateGray;
            this.textBoxAdaugMarca.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAdaugMarca.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.textBoxAdaugMarca.Location = new System.Drawing.Point(491, 52);
            this.textBoxAdaugMarca.Name = "textBoxAdaugMarca";
            this.textBoxAdaugMarca.Size = new System.Drawing.Size(210, 25);
            this.textBoxAdaugMarca.TabIndex = 13;
            this.textBoxAdaugMarca.Text = "Introduceti numele marcii";
            this.textBoxAdaugMarca.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxAdaugMarca.Enter += new System.EventHandler(this.textBoxAdaugMarca_Enter);
            // 
            // buttonStergeCateg
            // 
            this.buttonStergeCateg.BackColor = System.Drawing.Color.SlateGray;
            this.buttonStergeCateg.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonStergeCateg.BackgroundImage")));
            this.buttonStergeCateg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonStergeCateg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonStergeCateg.FlatAppearance.BorderSize = 0;
            this.buttonStergeCateg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStergeCateg.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStergeCateg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonStergeCateg.Location = new System.Drawing.Point(290, 268);
            this.buttonStergeCateg.Name = "buttonStergeCateg";
            this.buttonStergeCateg.Size = new System.Drawing.Size(195, 36);
            this.buttonStergeCateg.TabIndex = 7;
            this.buttonStergeCateg.Text = "Delete Category";
            this.buttonStergeCateg.UseVisualStyleBackColor = false;
            this.buttonStergeCateg.Click += new System.EventHandler(this.buttonStergeCateg_Click);
            // 
            // buttonStergeMarca
            // 
            this.buttonStergeMarca.BackColor = System.Drawing.Color.SlateGray;
            this.buttonStergeMarca.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonStergeMarca.BackgroundImage")));
            this.buttonStergeMarca.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonStergeMarca.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonStergeMarca.FlatAppearance.BorderSize = 0;
            this.buttonStergeMarca.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStergeMarca.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStergeMarca.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonStergeMarca.Location = new System.Drawing.Point(290, 110);
            this.buttonStergeMarca.Name = "buttonStergeMarca";
            this.buttonStergeMarca.Size = new System.Drawing.Size(175, 36);
            this.buttonStergeMarca.TabIndex = 6;
            this.buttonStergeMarca.Text = "Delete Brand";
            this.buttonStergeMarca.UseVisualStyleBackColor = false;
            this.buttonStergeMarca.Click += new System.EventHandler(this.buttonStergeMarca_Click);
            // 
            // buttonCateg
            // 
            this.buttonCateg.BackColor = System.Drawing.Color.SlateGray;
            this.buttonCateg.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonCateg.BackgroundImage")));
            this.buttonCateg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonCateg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCateg.FlatAppearance.BorderSize = 0;
            this.buttonCateg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonCateg.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCateg.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonCateg.Location = new System.Drawing.Point(290, 204);
            this.buttonCateg.Name = "buttonCateg";
            this.buttonCateg.Size = new System.Drawing.Size(195, 36);
            this.buttonCateg.TabIndex = 5;
            this.buttonCateg.Text = "Show Category";
            this.buttonCateg.UseVisualStyleBackColor = false;
            this.buttonCateg.Click += new System.EventHandler(this.buttonCateg_Click);
            // 
            // dataGridViewCateg
            // 
            this.dataGridViewCateg.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewCateg.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewCateg.BackgroundColor = System.Drawing.Color.SlateGray;
            this.dataGridViewCateg.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewCateg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCateg.Location = new System.Drawing.Point(17, 308);
            this.dataGridViewCateg.MultiSelect = false;
            this.dataGridViewCateg.Name = "dataGridViewCateg";
            this.dataGridViewCateg.ReadOnly = true;
            this.dataGridViewCateg.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewCateg.Size = new System.Drawing.Size(264, 212);
            this.dataGridViewCateg.TabIndex = 4;
            // 
            // buttonMarci
            // 
            this.buttonMarci.BackColor = System.Drawing.Color.SlateGray;
            this.buttonMarci.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonMarci.BackgroundImage")));
            this.buttonMarci.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonMarci.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonMarci.FlatAppearance.BorderSize = 0;
            this.buttonMarci.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMarci.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMarci.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonMarci.Location = new System.Drawing.Point(290, 45);
            this.buttonMarci.Name = "buttonMarci";
            this.buttonMarci.Size = new System.Drawing.Size(183, 36);
            this.buttonMarci.TabIndex = 3;
            this.buttonMarci.Text = "Show Brands";
            this.buttonMarci.UseVisualStyleBackColor = false;
            this.buttonMarci.Click += new System.EventHandler(this.buttonMarci_Click_1);
            // 
            // dataGridViewMarci
            // 
            this.dataGridViewMarci.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewMarci.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewMarci.BackgroundColor = System.Drawing.Color.SlateGray;
            this.dataGridViewMarci.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewMarci.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewMarci.Location = new System.Drawing.Point(17, 34);
            this.dataGridViewMarci.MultiSelect = false;
            this.dataGridViewMarci.Name = "dataGridViewMarci";
            this.dataGridViewMarci.ReadOnly = true;
            this.dataGridViewMarci.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewMarci.Size = new System.Drawing.Size(264, 231);
            this.dataGridViewMarci.TabIndex = 2;
            // 
            // panelProducts
            // 
            this.panelProducts.BackColor = System.Drawing.Color.SlateGray;
            this.panelProducts.Controls.Add(this.textBoxNumeMarca);
            this.panelProducts.Controls.Add(this.textBoxNumeCategorie);
            this.panelProducts.Controls.Add(this.textBoxPretProd);
            this.panelProducts.Controls.Add(this.textBoxCantitateProd);
            this.panelProducts.Controls.Add(this.textBoxDescriereProd);
            this.panelProducts.Controls.Add(this.textBoxNumeProd);
            this.panelProducts.Controls.Add(this.buttonInsert);
            this.panelProducts.Controls.Add(this.buttonDelete);
            this.panelProducts.Controls.Add(this.buttonUpdateProducts);
            this.panelProducts.Controls.Add(this.dataGridViewProduse);
            this.panelProducts.Controls.Add(this.buttonSearchProduct);
            this.panelProducts.Controls.Add(this.textBoxSearchProduct);
            this.panelProducts.Controls.Add(this.label1);
            this.panelProducts.Location = new System.Drawing.Point(628, 43);
            this.panelProducts.Name = "panelProducts";
            this.panelProducts.Size = new System.Drawing.Size(551, 590);
            this.panelProducts.TabIndex = 13;
            this.panelProducts.Visible = false;
            // 
            // textBoxNumeMarca
            // 
            this.textBoxNumeMarca.BackColor = System.Drawing.Color.SlateGray;
            this.textBoxNumeMarca.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNumeMarca.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.textBoxNumeMarca.Location = new System.Drawing.Point(29, 256);
            this.textBoxNumeMarca.Name = "textBoxNumeMarca";
            this.textBoxNumeMarca.Size = new System.Drawing.Size(157, 25);
            this.textBoxNumeMarca.TabIndex = 23;
            this.textBoxNumeMarca.Text = "Nume Marca";
            this.textBoxNumeMarca.Enter += new System.EventHandler(this.textBoxMarcaId_Enter);
            // 
            // textBoxNumeCategorie
            // 
            this.textBoxNumeCategorie.BackColor = System.Drawing.Color.SlateGray;
            this.textBoxNumeCategorie.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNumeCategorie.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.textBoxNumeCategorie.Location = new System.Drawing.Point(28, 292);
            this.textBoxNumeCategorie.Name = "textBoxNumeCategorie";
            this.textBoxNumeCategorie.Size = new System.Drawing.Size(158, 25);
            this.textBoxNumeCategorie.TabIndex = 22;
            this.textBoxNumeCategorie.Text = "Nume Categorie ";
            this.textBoxNumeCategorie.Enter += new System.EventHandler(this.textBoxCategorieId_Enter);
            // 
            // textBoxPretProd
            // 
            this.textBoxPretProd.BackColor = System.Drawing.Color.LightSlateGray;
            this.textBoxPretProd.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPretProd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.textBoxPretProd.Location = new System.Drawing.Point(29, 490);
            this.textBoxPretProd.Name = "textBoxPretProd";
            this.textBoxPretProd.Size = new System.Drawing.Size(157, 25);
            this.textBoxPretProd.TabIndex = 21;
            this.textBoxPretProd.Text = "Pret Produs";
            this.textBoxPretProd.Enter += new System.EventHandler(this.textBoxPretProd_Enter);
            // 
            // textBoxCantitateProd
            // 
            this.textBoxCantitateProd.BackColor = System.Drawing.Color.SlateGray;
            this.textBoxCantitateProd.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxCantitateProd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.textBoxCantitateProd.Location = new System.Drawing.Point(29, 439);
            this.textBoxCantitateProd.Name = "textBoxCantitateProd";
            this.textBoxCantitateProd.Size = new System.Drawing.Size(157, 25);
            this.textBoxCantitateProd.TabIndex = 20;
            this.textBoxCantitateProd.Text = "Cantitate Produs";
            this.textBoxCantitateProd.Enter += new System.EventHandler(this.textBoxCantitateProd_Enter);
            // 
            // textBoxDescriereProd
            // 
            this.textBoxDescriereProd.BackColor = System.Drawing.Color.SlateGray;
            this.textBoxDescriereProd.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxDescriereProd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.textBoxDescriereProd.Location = new System.Drawing.Point(29, 379);
            this.textBoxDescriereProd.Multiline = true;
            this.textBoxDescriereProd.Name = "textBoxDescriereProd";
            this.textBoxDescriereProd.Size = new System.Drawing.Size(157, 35);
            this.textBoxDescriereProd.TabIndex = 19;
            this.textBoxDescriereProd.Text = "Descriere Produs";
            this.textBoxDescriereProd.Enter += new System.EventHandler(this.textBoxDescriereProd_Enter);
            // 
            // textBoxNumeProd
            // 
            this.textBoxNumeProd.BackColor = System.Drawing.Color.SlateGray;
            this.textBoxNumeProd.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxNumeProd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.textBoxNumeProd.Location = new System.Drawing.Point(29, 333);
            this.textBoxNumeProd.Name = "textBoxNumeProd";
            this.textBoxNumeProd.Size = new System.Drawing.Size(157, 25);
            this.textBoxNumeProd.TabIndex = 18;
            this.textBoxNumeProd.Text = "Nume produs";
            this.textBoxNumeProd.Enter += new System.EventHandler(this.textBoxNumeProd_Enter);
            // 
            // buttonInsert
            // 
            this.buttonInsert.BackColor = System.Drawing.Color.SlateGray;
            this.buttonInsert.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonInsert.BackgroundImage")));
            this.buttonInsert.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonInsert.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonInsert.FlatAppearance.BorderSize = 0;
            this.buttonInsert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInsert.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonInsert.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonInsert.Location = new System.Drawing.Point(270, 479);
            this.buttonInsert.Name = "buttonInsert";
            this.buttonInsert.Size = new System.Drawing.Size(211, 36);
            this.buttonInsert.TabIndex = 14;
            this.buttonInsert.Text = "Insert Product";
            this.buttonInsert.UseVisualStyleBackColor = false;
            this.buttonInsert.Click += new System.EventHandler(this.buttonInsert_Click);
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.Color.SlateGray;
            this.buttonDelete.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonDelete.BackgroundImage")));
            this.buttonDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonDelete.FlatAppearance.BorderSize = 0;
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonDelete.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDelete.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonDelete.Location = new System.Drawing.Point(270, 213);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(127, 35);
            this.buttonDelete.TabIndex = 13;
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // buttonUpdateProducts
            // 
            this.buttonUpdateProducts.BackColor = System.Drawing.Color.SlateGray;
            this.buttonUpdateProducts.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonUpdateProducts.BackgroundImage")));
            this.buttonUpdateProducts.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonUpdateProducts.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonUpdateProducts.FlatAppearance.BorderSize = 0;
            this.buttonUpdateProducts.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonUpdateProducts.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUpdateProducts.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonUpdateProducts.Location = new System.Drawing.Point(28, 212);
            this.buttonUpdateProducts.Name = "buttonUpdateProducts";
            this.buttonUpdateProducts.Size = new System.Drawing.Size(198, 36);
            this.buttonUpdateProducts.TabIndex = 12;
            this.buttonUpdateProducts.Text = "Update details";
            this.buttonUpdateProducts.UseVisualStyleBackColor = false;
            this.buttonUpdateProducts.Click += new System.EventHandler(this.buttonUpdateProducts_Click);
            // 
            // dataGridViewProduse
            // 
            this.dataGridViewProduse.AllowDrop = true;
            this.dataGridViewProduse.AllowUserToOrderColumns = true;
            this.dataGridViewProduse.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridViewProduse.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.DisplayedCells;
            this.dataGridViewProduse.BackgroundColor = System.Drawing.Color.SlateGray;
            this.dataGridViewProduse.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dataGridViewProduse.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewProduse.Location = new System.Drawing.Point(17, 48);
            this.dataGridViewProduse.Name = "dataGridViewProduse";
            this.dataGridViewProduse.Size = new System.Drawing.Size(500, 150);
            this.dataGridViewProduse.TabIndex = 8;
            this.dataGridViewProduse.VirtualMode = true;
            // 
            // buttonSearchProduct
            // 
            this.buttonSearchProduct.BackColor = System.Drawing.Color.SlateGray;
            this.buttonSearchProduct.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonSearchProduct.BackgroundImage")));
            this.buttonSearchProduct.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonSearchProduct.FlatAppearance.BorderSize = 0;
            this.buttonSearchProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSearchProduct.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSearchProduct.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonSearchProduct.Location = new System.Drawing.Point(375, 3);
            this.buttonSearchProduct.Name = "buttonSearchProduct";
            this.buttonSearchProduct.Size = new System.Drawing.Size(128, 36);
            this.buttonSearchProduct.TabIndex = 5;
            this.buttonSearchProduct.Text = "Search";
            this.buttonSearchProduct.UseVisualStyleBackColor = false;
            this.buttonSearchProduct.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBoxSearchProduct
            // 
            this.textBoxSearchProduct.BackColor = System.Drawing.Color.SlateGray;
            this.textBoxSearchProduct.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.textBoxSearchProduct.Location = new System.Drawing.Point(206, 10);
            this.textBoxSearchProduct.Name = "textBoxSearchProduct";
            this.textBoxSearchProduct.Size = new System.Drawing.Size(153, 20);
            this.textBoxSearchProduct.TabIndex = 7;
            this.textBoxSearchProduct.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.label1.Location = new System.Drawing.Point(14, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(172, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Search for a product:";
            // 
            // panelUserDetails
            // 
            this.panelUserDetails.BackColor = System.Drawing.Color.SlateGray;
            this.panelUserDetails.Controls.Add(this.button_delete_order);
            this.panelUserDetails.Controls.Add(this.button1);
            this.panelUserDetails.Controls.Add(this.label_quantity);
            this.panelUserDetails.Controls.Add(this.textBox_quantity);
            this.panelUserDetails.Controls.Add(this.button_stergere);
            this.panelUserDetails.Controls.Add(this.buttonQuantity);
            this.panelUserDetails.Controls.Add(this.searchUser);
            this.panelUserDetails.Controls.Add(this.dataGridViewAdmin);
            this.panelUserDetails.Controls.Add(this.textBoxUsername);
            this.panelUserDetails.Controls.Add(this.labelUsername);
            this.panelUserDetails.Location = new System.Drawing.Point(3, 33);
            this.panelUserDetails.Name = "panelUserDetails";
            this.panelUserDetails.Size = new System.Drawing.Size(601, 350);
            this.panelUserDetails.TabIndex = 14;
            this.panelUserDetails.Visible = false;
            // 
            // button_delete_order
            // 
            this.button_delete_order.BackColor = System.Drawing.Color.SlateGray;
            this.button_delete_order.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button_delete_order.BackgroundImage")));
            this.button_delete_order.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_delete_order.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_delete_order.FlatAppearance.BorderSize = 0;
            this.button_delete_order.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_delete_order.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_delete_order.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.button_delete_order.Location = new System.Drawing.Point(397, 282);
            this.button_delete_order.Name = "button_delete_order";
            this.button_delete_order.Size = new System.Drawing.Size(183, 36);
            this.button_delete_order.TabIndex = 12;
            this.button_delete_order.Text = "Delete Order";
            this.button_delete_order.UseVisualStyleBackColor = false;
            this.button_delete_order.Click += new System.EventHandler(this.button_delete_order_Click);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.SlateGray;
            this.button1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button1.BackgroundImage")));
            this.button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.button1.Location = new System.Drawing.Point(9, 282);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(191, 36);
            this.button1.TabIndex = 10;
            this.button1.Text = "Select Product";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label_quantity
            // 
            this.label_quantity.AutoSize = true;
            this.label_quantity.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_quantity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.label_quantity.Location = new System.Drawing.Point(2, 397);
            this.label_quantity.Name = "label_quantity";
            this.label_quantity.Size = new System.Drawing.Size(119, 17);
            this.label_quantity.TabIndex = 8;
            this.label_quantity.Text = "New quantity:";
            // 
            // textBox_quantity
            // 
            this.textBox_quantity.BackColor = System.Drawing.Color.SlateGray;
            this.textBox_quantity.Location = new System.Drawing.Point(127, 397);
            this.textBox_quantity.Name = "textBox_quantity";
            this.textBox_quantity.Size = new System.Drawing.Size(153, 20);
            this.textBox_quantity.TabIndex = 7;
            // 
            // button_stergere
            // 
            this.button_stergere.BackColor = System.Drawing.Color.SlateGray;
            this.button_stergere.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button_stergere.BackgroundImage")));
            this.button_stergere.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.button_stergere.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button_stergere.FlatAppearance.BorderSize = 0;
            this.button_stergere.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_stergere.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_stergere.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.button_stergere.Location = new System.Drawing.Point(232, 282);
            this.button_stergere.Name = "button_stergere";
            this.button_stergere.Size = new System.Drawing.Size(125, 36);
            this.button_stergere.TabIndex = 11;
            this.button_stergere.Text = "Delete";
            this.button_stergere.UseVisualStyleBackColor = false;
            this.button_stergere.Click += new System.EventHandler(this.button_stergere_Click);
            // 
            // buttonQuantity
            // 
            this.buttonQuantity.BackColor = System.Drawing.Color.SlateGray;
            this.buttonQuantity.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonQuantity.BackgroundImage")));
            this.buttonQuantity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonQuantity.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonQuantity.FlatAppearance.BorderSize = 0;
            this.buttonQuantity.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonQuantity.Font = new System.Drawing.Font("Cooper Black", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonQuantity.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.buttonQuantity.Location = new System.Drawing.Point(299, 389);
            this.buttonQuantity.Name = "buttonQuantity";
            this.buttonQuantity.Size = new System.Drawing.Size(193, 33);
            this.buttonQuantity.TabIndex = 6;
            this.buttonQuantity.Text = "Edit Quantity";
            this.buttonQuantity.UseVisualStyleBackColor = false;
            this.buttonQuantity.Click += new System.EventHandler(this.buttonQuantity_Click);
            // 
            // label_cantitate
            // 
            this.label_cantitate.AutoSize = true;
            this.label_cantitate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.label_cantitate.Location = new System.Drawing.Point(704, 319);
            this.label_cantitate.Name = "label_cantitate";
            this.label_cantitate.Size = new System.Drawing.Size(0, 13);
            this.label_cantitate.TabIndex = 9;
            // 
            // label_search_error
            // 
            this.label_search_error.AutoSize = true;
            this.label_search_error.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.label_search_error.Location = new System.Drawing.Point(447, 42);
            this.label_search_error.Name = "label_search_error";
            this.label_search_error.Size = new System.Drawing.Size(0, 13);
            this.label_search_error.TabIndex = 5;
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.SlateGray;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1355, 24);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionareComenziUserToolStripMenuItem,
            this.gestionareProduseToolStripMenuItem,
            this.marciCategoriiToolStripMenuItem});
            this.menuToolStripMenuItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.menuToolStripMenuItem.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(225)))), ((int)(((byte)(225)))), ((int)(((byte)(225)))));
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(57, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // gestionareComenziUserToolStripMenuItem
            // 
            this.gestionareComenziUserToolStripMenuItem.Name = "gestionareComenziUserToolStripMenuItem";
            this.gestionareComenziUserToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.gestionareComenziUserToolStripMenuItem.Text = "Gestionare Comenzi User";
            this.gestionareComenziUserToolStripMenuItem.Click += new System.EventHandler(this.gestionareComenziUserToolStripMenuItem_Click);
            // 
            // gestionareProduseToolStripMenuItem
            // 
            this.gestionareProduseToolStripMenuItem.Name = "gestionareProduseToolStripMenuItem";
            this.gestionareProduseToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.gestionareProduseToolStripMenuItem.Text = "Gestionare Produse";
            this.gestionareProduseToolStripMenuItem.Click += new System.EventHandler(this.gestionareProduseToolStripMenuItem_Click);
            // 
            // marciCategoriiToolStripMenuItem
            // 
            this.marciCategoriiToolStripMenuItem.Name = "marciCategoriiToolStripMenuItem";
            this.marciCategoriiToolStripMenuItem.Size = new System.Drawing.Size(252, 22);
            this.marciCategoriiToolStripMenuItem.Text = "Marci & Categorii";
            this.marciCategoriiToolStripMenuItem.Click += new System.EventHandler(this.marciCategoriiToolStripMenuItem_Click);
            // 
            // FormAdmin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(1355, 614);
            this.Controls.Add(this.panel_right);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormAdmin";
            this.Text = "FormAdmin";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewAdmin)).EndInit();
            this.panel_right.ResumeLayout(false);
            this.panel_right.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelCategMarci.ResumeLayout(false);
            this.panelCategMarci.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCateg)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewMarci)).EndInit();
            this.panelProducts.ResumeLayout(false);
            this.panelProducts.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewProduse)).EndInit();
            this.panelUserDetails.ResumeLayout(false);
            this.panelUserDetails.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button searchUser;
        private System.Windows.Forms.DataGridView dataGridViewAdmin;
        private System.Windows.Forms.Label labelUsername;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.Panel panel_right;
        private System.Windows.Forms.Label wrong_dates;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label_search_error;
        private System.Windows.Forms.Button buttonQuantity;
        private System.Windows.Forms.Label label_quantity;
        private System.Windows.Forms.TextBox textBox_quantity;
        private System.Windows.Forms.Label label_cantitate;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_stergere;
        private System.Windows.Forms.Button button_delete_order;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionareComenziUserToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionareProduseToolStripMenuItem;
        private System.Windows.Forms.Panel panelUserDetails;
        private System.Windows.Forms.Panel panelProducts;
        private System.Windows.Forms.Button buttonSearchProduct;
        private System.Windows.Forms.TextBox textBoxSearchProduct;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonUpdateProducts;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Button buttonInsert;
        private System.Windows.Forms.DataGridView dataGridViewProduse;
        private System.Windows.Forms.TextBox textBoxPretProd;
        private System.Windows.Forms.TextBox textBoxCantitateProd;
        private System.Windows.Forms.TextBox textBoxDescriereProd;
        private System.Windows.Forms.TextBox textBoxNumeProd;
        private System.Windows.Forms.TextBox textBoxNumeMarca;
        private System.Windows.Forms.TextBox textBoxNumeCategorie;
        private System.Windows.Forms.ToolStripMenuItem marciCategoriiToolStripMenuItem;
        private System.Windows.Forms.Panel panelCategMarci;
        private System.Windows.Forms.Button buttonMarci;
        private System.Windows.Forms.DataGridView dataGridViewMarci;
        private System.Windows.Forms.Button buttonCateg;
        private System.Windows.Forms.DataGridView dataGridViewCateg;
        private System.Windows.Forms.Button buttonStergeCateg;
        private System.Windows.Forms.Button buttonStergeMarca;
        private System.Windows.Forms.Button buttonAdaugareMarca;
        private System.Windows.Forms.Button buttonAdaugareCateg;
        private System.Windows.Forms.TextBox textBoxAdaugCateg;
        private System.Windows.Forms.TextBox textBoxAdaugMarca;
    }
}