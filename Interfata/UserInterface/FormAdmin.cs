﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserInterface
{
    public partial class FormAdmin : Form
    {
        DataBaseServiceReference.Cos[] cos = service.getCos();
        DataBaseServiceReference.Produse produs = null;
        DataBaseServiceReference.Produse p = null;
        static DataBaseServiceReference.WebServiceSoapClient service = new DataBaseServiceReference.WebServiceSoapClient();
        DataBaseServiceReference.Users user = null;
        
 
        Form1 form1;
        Elemente_cos element = new Elemente_cos();
        

        public FormAdmin()
        {
            InitializeComponent();
        }

        public void refreshCos()
        {
            List<Elemente_cos> elemente_Cos = new List<Elemente_cos>();
            user = service.getUserByEmail(textBoxUsername.Text);
            try
            {
                cos = service.getCosByUserId(user.user_id);
                
                    foreach (DataBaseServiceReference.Cos c in cos)
                    {
                        Elemente_cos ec = new Elemente_cos();
                        produs = service.getProdusById(c.produs_id);
                        ec.NumeProdus = produs.nume;
                        ec.Marca = service.getMarcaById(produs.marca_id).nume;
                        ec.Categorie = service.getCategorieById(produs.categorii_id).nume;
                        ec.Cantitate = c.cant_cos;
                        ec.Pret = c.cant_cos * produs.pret;

                        elemente_Cos.Add(ec);
                        
                    }
                label_search_error.Text = "";
                dataGridViewAdmin.DataSource = elemente_Cos;
                    dataGridViewAdmin.Show();
                
            }
            catch (Exception ex)
            {
                label_search_error.Text = "Username not found";
                dataGridViewAdmin.Hide();
            }
        }
        private void searchUser_Click(object sender, EventArgs e)
        {
            refreshCos();
        }

        private void quit_Click_1(object sender, EventArgs e)
        {
            this.Close();
            form1 = new Form1();
            form1.Show();
        }

        private void buttonQuantity_Click(object sender, EventArgs e)
        {
            try
            {
                int quantity = Convert.ToInt32(textBox_quantity.Text);
                p = service.getProdusIdByNumeProdus(element.NumeProdus);
                int q = 0;
                q = p.cantitate - quantity + element.Cantitate;

                if (q >= 0)
                {
                    service.ModificareCantitateCos(user.user_id, quantity, p.produs_id);
                    service.ModificareCantitateProdus(p.produs_id, q);
                    refreshCos();
                }
                else
                {
                    label_cantitate.Text = "Stoc insuficient";
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Eroare ");
            }
        }

        
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                p = service.getProdusIdByNumeProdus(element.NumeProdus);
                DataGridViewRow dgv = dataGridViewAdmin.CurrentRow;
                int Index = dataGridViewAdmin.CurrentCell.RowIndex;
                
                if (dgv != null)
                {
                    element.NumeProdus = Convert.ToString(dgv.Cells[0].Value);
                    element.Marca = Convert.ToString(dgv.Cells[1].Value);
                    element.Categorie = Convert.ToString(dgv.Cells[2].Value);
                    element.Cantitate = Convert.ToInt32(dgv.Cells[3].Value);
                    element.Pret = Convert.ToInt32(dgv.Cells[4].Value);
                    label_cantitate.Text = "";
                    label_cantitate.Text = "Product " + element.NumeProdus.ToString() + " selected";
                }
                else
                {
                    MessageBox.Show("Please Select Product");
                }
            }
            catch (Exception ex)
            {
                label_cantitate.Text = "Please select a product.";
            }
        }

        private void button_stergere_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow dgv = dataGridViewAdmin.CurrentRow;
                element.NumeProdus = Convert.ToString(dgv.Cells[0].Value);
                p = service.getProdusIdByNumeProdus(element.NumeProdus);
                MessageBox.Show(p.produs_id.ToString());
                service.ModificareCantitateProdus(p.produs_id, p.cantitate + element.Cantitate);
                if (service.StergereProdusDinCos(service.getProdusIdByNumeProdus(element.NumeProdus).produs_id,user.user_id)==1)
                    label_cantitate.Text = "Product deleted";
                else
                    label_cantitate.Text = "stergere nereuista";
               
                refreshCos();
                
            }
            catch(Exception ex)
            {
                label_cantitate.Text = "Eroare stergere";
            }
        }

        private void button_delete_order_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow dgv = dataGridViewAdmin.CurrentRow;
                DataBaseServiceReference.Users u = service.getUserByEmail(textBoxUsername.Text);
                if (service.StergereCos(u.user_id) == 1)
                    label_cantitate.Text = "Orderd deleted";
                else
                    label_cantitate.Text = "stergere nereuista";

                refreshCos();

            }
            catch (Exception ex)
            {
                label_cantitate.Text = "Eroare stergere";
            }
        }

        private void gestionareComenziUserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelUserDetails.Visible = true;
            panelProducts.Visible = false;
            panelCategMarci.Visible = false;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            dataGridViewProduse.DataSource = service.getProduseByNume(textBoxSearchProduct.Text);
        }

        private void gestionareProduseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelProducts.Visible = true;
            panelProducts.BringToFront();
            panelProducts.Location=new Point(10, 25);
            panelUserDetails.Visible = false;
            panelCategMarci.Visible = false;
        }

        private void buttonUpdateProducts_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow dgv = dataGridViewProduse.CurrentRow;
                int produsId = Convert.ToInt32(dgv.Cells[0].Value);
                int categoriiId = Convert.ToInt32(dgv.Cells[1].Value);
                int marcaId = Convert.ToInt32(dgv.Cells[2].Value);
                string nume = dgv.Cells[3].Value.ToString();
                string descriere = dgv.Cells[4].Value.ToString();
                int cantitate = Convert.ToInt32(dgv.Cells[5].Value);
                double pret = Convert.ToDouble(dgv.Cells[6].Value);

                if (service.ModificareProdus(produsId, categoriiId, marcaId, nume, descriere, cantitate, pret) == 1)
                    MessageBox.Show("Produs Modificat cu succes");
                else
                    MessageBox.Show("Eroare modificare produs");
            }catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow dgv = dataGridViewProduse.CurrentRow;
                int produsId = Convert.ToInt32(dgv.Cells[0].Value);
                if (service.StergereProdus(produsId) == 1)
                    MessageBox.Show("Produs Sters cu succes");
                else
                    MessageBox.Show("Eroare stergere produs");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            try
            {
                int produsId = Convert.ToInt32(service.getIndiceMaxProduse());
                int categoriiId =service.getIdCategByNumeCateg(textBoxNumeCategorie.Text);
                int marcaId = service.getIdMarcaByNumeMarca(textBoxNumeMarca.Text);
                string nume = textBoxNumeProd.Text.ToString();
                string descriere = textBoxDescriereProd.Text.ToString();
                int cantitate = Convert.ToInt32(textBoxCantitateProd.Text);
                double pret = Convert.ToDouble(textBoxPretProd.Text);
                if (service.InsertProduse(produsId+1, categoriiId+1, marcaId+1, nume, descriere, cantitate, pret) == 1)
                    MessageBox.Show("Produs Inserat cu succes");
                else
                    MessageBox.Show("Eroare inserare produs");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void textBoxMarcaId_Enter(object sender, EventArgs e)
        {
            textBoxNumeMarca.Text = "";
        }

        private void textBoxCategorieId_Enter(object sender, EventArgs e)
        {
            textBoxNumeCategorie.Text = "";
        }

        private void textBoxNumeProd_Enter(object sender, EventArgs e)
        {
            textBoxNumeProd.Text = "";
        }

        private void textBoxDescriereProd_Enter(object sender, EventArgs e)
        {
            textBoxDescriereProd.Text = "";
        }

        private void textBoxCantitateProd_Enter(object sender, EventArgs e)
        {
            textBoxCantitateProd.Text = "";
        }

        private void textBoxPretProd_Enter(object sender, EventArgs e)
        {
            textBoxPretProd.Text = "";
        }

        private void marciCategoriiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            panelCategMarci.Location = new Point(10, 25);
            panelCategMarci.Size = new Size(900, 900);
            panelCategMarci.Visible = true;
            panelProducts.Visible = false;
            panelUserDetails.Visible = false;
        }

        private void buttonMarci_Click(object sender, EventArgs e)
        {
            dataGridViewMarci.DataSource = service.getMarci();
        }

        private void buttonCateg_Click(object sender, EventArgs e)
        {
            dataGridViewCateg.DataSource = service.getCategorii();
        }

        private void buttonStergeMarca_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow dgv = dataGridViewMarci.CurrentRow;
                int marcaId = Convert.ToInt32(dgv.Cells[0].Value);
                if (service.StergereMarca(marcaId) == 1)
                    MessageBox.Show("Marca stearsa cu succes");
                else
                    MessageBox.Show("Eroare stergere");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void buttonStergeCateg_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridViewRow dgv = dataGridViewCateg.CurrentRow;
                int categId = Convert.ToInt32(dgv.Cells[0].Value);
                if (service.StergereCategorii(categId) == 1)
                    MessageBox.Show("Categoria cu id "+categId+" stearsa cu succes");
                else
                    MessageBox.Show("Eroare stergere ");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void buttonAdaugareMarca_Click(object sender, EventArgs e)
        {
            try {
                int marcaId = service.getIndiceMaxMarci() + 1;
                string nume = textBoxAdaugMarca.Text.ToString();
                if(service.InsertMarci(marcaId,nume)==1)
                {
                    MessageBox.Show("Marca "+nume+" Inserata cu succes");
                }
                else
                { MessageBox.Show("Verificati detaliile introduse."); }
            } catch(Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        private void buttonAdaugareCateg_Click(object sender, EventArgs e)
        {
            try
            {
                int categId = service.getIndiceMaxCategorii() + 1;
                string nume = textBoxAdaugCateg.Text.ToString();
                if (service.InsertCategorii(categId, nume) == 1)
                {
                    MessageBox.Show("Categoria "+nume+" Inserata cu succes");
                }
                else
                { MessageBox.Show("Verificati detaliile introduse."); }
            }
            catch (Exception ex)
            { MessageBox.Show(ex.Message); }
        }

        private void textBoxAdaugMarca_Enter(object sender, EventArgs e)
        {
            textBoxAdaugMarca.Text = "";
        }

        private void textBoxAdaugCateg_Enter(object sender, EventArgs e)
        {
            textBoxAdaugCateg.Text = "";
        }

        private void textBoxUsername_TextChanged(object sender, EventArgs e)
        {
            refreshCos();
        }

        private void buttonMarci_Click_1(object sender, EventArgs e)
        {
            dataGridViewMarci.DataSource = service.getMarci();
        }
    }
}
