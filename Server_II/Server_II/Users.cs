﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server_II
{
    public class Users
    {
        public int user_id { get; set; }
        public string email { get; set; }
        public string pass { get; set; }
        public int is_admin { get; set; }
    }
}