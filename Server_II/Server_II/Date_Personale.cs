﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server_II
{
    public class Date_Personale
    {
        public int date_personale_id { get; set; }
        public string nume { get; set; }
        public string prenume { get; set; }
        public string adresa { get; set; }
        public string telefon { get; set; }
    }
}