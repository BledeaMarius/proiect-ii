﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Server_II
{
    public class Produse
    {
        public int produs_id { get; set; }
        public int categorii_id { get; set; }
        public int marca_id { get; set; }
        public string nume { get; set; }
        public string descriere { get; set; }
        public int cantitate { get; set; }
        public double pret { get; set; }
    }
}